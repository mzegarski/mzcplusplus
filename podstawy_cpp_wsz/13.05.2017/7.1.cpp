#include <iostream>
using namespace std;

const int L = 10; //rozmiar tablicy

int main()
{
	char t[L] = "aB CD eF";
	
	cout <<"Wpisano: " << t << endl;
	
	if (t[0]>= 97 && t[0]<=122) t[0] = t[0] -32; //zamiana pierwszego znaku na duzy o ile jest maly
	
	for (int i=1; i<L;i++)
	{
		if (t[i]==32)
		{
			i++;
			if (t[i]>= 97 && t[i]<=122) t[i] = t[i] -32;
			continue;
		}
		else if (t[i]>= 65 && t[i]<=90) t[i] = t[i] +32;
	}
	
	cout <<"Skonwertowano: " << t << endl;
	
	return 0;
	
}
