#include <iostream>

using namespace std;

const int N = 7;
const int MAXP = 99999;

int tab[7][7] = {{0, 15, 17, 0, 0, 0, 0},
  {0, 0, 0, 13, 20, 0, 0}, 
  {0, 0, 0, 12, 0, 11, 0},
  {0, 0, 0, 0, 12, 16, 0},
  {0, 0, 0, 0, 0, 0, 10},
  {0, 0, 0, 0, 0, 0, 14},
  {0, 0, 0, 0, 0, 0, 0}};
  
int mmin(int x, int y) {
    return x<y ? x : y;
}

int mmax(int x, int y) {
    return x>y ? x : y;
}
  
int sciezka(int nr) {
    int p = MAXP;
    
    for(int i=0; i<N; ++i) {
        if (tab[nr][i]>0) {
            int s = mmin(sciezka(i), tab[nr][i]);
            if (p == MAXP) {
                p = s;
            }
            else {
                p = mmax(p, s);
            }
        }
    }
    
    return p;
}

int main(int argc, char** argv) {
    
    cout << sciezka(0);
    
    system("pause");
	return 0;
}
