#include <iostream>

using namespace std; 

int main ()
{
	int tab[5]= {0, 1, 2, 3, 4};
	int *w = tab;
	
	
	for(int i=0; i<5 ; i++) cout <<tab[i]<<endl;
	
	cout <<endl;
	
	for(int i=0; i<5 ; i++)
	{
		cout << *w <<endl;
		w++;
	} 
	
	cout <<endl;
	
	
	
	//tak powinno sie robic:
	
	int * first = tab; //1 element w tablicy
	int * last = tab+5; //1 pozycja za ostatnim elemenetem tablicy
	for (int * w = first ; w!=last; w++) cout << *w <<endl;
	
	
	return 0;
	
	
}
