#include <iostream>

using namespace std; 

void foo1 (int a);
void foo2 (int *a);
void foo3 (int &a);

int main ()
{
	int a=5;
	
	cout << a <<endl;
	
	//foo1 (a);
	
	foo2 (&a);
	
	//foo3 (a);
	
	cout << a <<endl;
	
	return 0;
	
	
	
}

void foo1 (int a)
{
	cout << a <<endl;
	a++;
	cout << a <<endl;
}

void foo2 (int *a)
{
	cout << *a <<endl;
	(*a)++;
	cout << *a <<endl;
}

void foo3 (int &a)
{
	cout << a <<endl;
	a++;
	cout << a <<endl;
}
