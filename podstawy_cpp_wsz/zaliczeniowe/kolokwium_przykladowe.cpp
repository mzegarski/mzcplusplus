#include <iostream>
#include <fstream>

using namespace std;

const int N = 100; //rozmiar wczytywanych naglowkow

int main()
{
    ifstream file;

    file.open ("pojemniki.txt");
    char naglowek_1 [N];
    file >> naglowek_1;
    int liczba_pojemnikow;
    file >> liczba_pojemnikow;
    char naglowek_2 [N];
    file >> naglowek_2;
    int pojemniki [liczba_pojemnikow];
    for (int i=0; i<liczba_pojemnikow; i++)
    {
        file >> pojemniki[i];
    }
    file.close();

    file.open ("towary.txt");
    char naglowek_3 [N];
    file >> naglowek_3;
    int liczba_towarow;
    file >> liczba_towarow;
    char naglowek_4 [N];
    file >> naglowek_4;
    int towary [liczba_towarow];
    for (int i=0; i<liczba_towarow; i++)
    {
        file >> towary[i];
    }
    file.close();

    //opcjonolne wypisywanie pliku "pojemniki.txt"
    cout << naglowek_1<<endl;
    cout << liczba_pojemnikow<<endl;
    cout << naglowek_2<<endl;
    for (int i=0; i<liczba_pojemnikow; i++)
    {
        cout << pojemniki[i] <<" ";
    }
    cout <<endl<<endl;

    //opcjonalne wypisywanie pliku "towary.txt"
    cout << naglowek_3<<endl;
    cout << liczba_towarow<<endl;
    cout << naglowek_4<<endl;
    for (int i=0; i<liczba_towarow; i++)
    {
        cout << towary[i] <<" ";
    }
    cout <<endl<<endl;

    //rozdzielanie towarow po pojemnikach i wypisywanie komunikatow
    for (int i=0; i<liczba_towarow; i++)
    {
        for (int j=0; j<liczba_pojemnikow; j++)
        {
            if (towary[i]<=pojemniki[j])
            {
                pojemniki[j]-=towary[i];
                cout << "towar "<<i+1<<" wlozony do pojemnika "<<j+1<<endl;
                break;
            }
            else if (j==liczba_pojemnikow-1)
            {
                cout <<"towar " <<i+1<<" nie miesci sie do pojemnikow. "<<endl;
            }
        }
    }

    //opcjonalne wypisywanie wyniku
    cout <<endl<<"Wolne miejsce w pojemnikach:"<<endl;
    for (int i=0; i<liczba_pojemnikow; i++)
    {
        cout <<"Pojemnik "<<i+1<<": "<< pojemniki[i] <<endl;
    }

    //zapisywanie wyniku do pliku
    ofstream save;
    save.open ("raport.txt");
    char pojemnik [9]="Pojemnik";
    for (int i=0; i<liczba_pojemnikow; i++)
    {
        save <<pojemnik<<" "<< i+1<<": "<<pojemniki[i]<<endl;
    }

    save.close();

    return 0;
}
