//Dla danej tablicy liczb ca�kowitych wska� warto�� najmniejsz�, najwi�ksz� oraz oblicz �redni� arytmetyczn� i geometryczn�.

#include <iostream>
#include <math.h>

using namespace std;

int main()
{
	double tab[10] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
	double n = 10.0;
	
	double najmniejsza = tab [0];
	double najwieksza = tab [0];
	double suma = 0;
	double iloczyn =1;
	long double srednia_ary, srednia_geo;
	
	for (int i=0; i<n; i++)
	{
		if (tab[i]< najmniejsza)
		{
			najmniejsza = tab[i];
		}
	}
	
	for (int i=0; i<n; i++)
	{
		if (tab[i]> najwieksza)
		{
			najwieksza = tab[i];
		}
	}
	
	for (int i=0; i<n; i++)
	{
		suma = suma + tab[i];
	}
	
	srednia_ary = suma/n;
	
	for (int i=0; i<n; i++)
	{
		iloczyn = iloczyn * tab[i];
	}
	
	srednia_geo = pow (iloczyn, (1/n));
	
	cout <<"Najmniejsza liczba w tablicy to: " << najmniejsza << endl;
	cout <<"Najwieksza liczba w tablicy to: " << najwieksza << endl;
	cout <<"Suma liczb w tablicy to: " << suma << endl;
	cout <<"Srednia arytmetyczna liczb w tablicy to: " << srednia_ary << endl;
	cout <<"Srednia geometryczna liczb w tablicy to: " << srednia_geo << endl;
	
	return 0;
	
}
