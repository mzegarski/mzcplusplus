/* Dane s� trzy punkty w uk�adzie kartezja�skim x/y
sprawdz czy s� wierzcho�kami tr�jkata r�wnobocznego*/

#include <iostream>
#include <math.h>
using namespace std;

double x[3], y[3]; //wspolrzedne punktow TU MOGA BYC CZWORKI

double odleglosc(double x1, double y1, double x2, double y2)
{
	//odleglosc miedzy punktami
	double d = sqrt( ((x2 - x1)*(x2 - x1)) + ((y2 - y1)*(y2 - y1)) );
	return d;
}

void pobieranie(int i)
{
	cout <<"Podaj wspolrzedna nr "<< i <<":"<<endl;
	cout <<"Podaj x: ";
	cin >> x[i];
	cout <<"Podaj y: ";
	cin >> y[i];
	return;	
}

int main()
{
	double odl_1, odl_2, odl_3;
	
	cout <<"Podaj trzy punkty w uk�adzie karteznanskim, program sprawdzi czy sa wierzcholkami trojkata rownobocznego" <<endl;
	for(int i = 1; i<=3; i++)
	{
		pobieranie(i);
	}
	
	odl_1 = odleglosc(x[1], y[1], x[2], y[2]);
	odl_2 = odleglosc(x[1], y[1], x[3], y[3]);
	odl_3 = odleglosc(x[2], y[2], x[3], y[3]);
	
	cout <<"Odleglosc 1: "<<odl_1 <<endl;
	cout <<"Odleglosc 2: "<<odl_2 <<endl;
	cout <<"Odleglosc 3: "<<odl_3 <<endl;
	
	if (odl_1 == odl_2 && odl_2 == odl_3)
	{
		if (odl_1 == 0 || odl_2 == 0 ||odl_3 == 0 )
		{
			cout <<"Przynajmniej dwa punkty na ten samej pozycji";
			return 0;
		}
		cout << "Trojkat JEST rownoboczny";
	}
	else
	{
		cout << "Trojkat NIE jest rownoboczny";
	}
		
	return 0;
	
}
