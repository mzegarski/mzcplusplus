#include <iostream>

using namespace std;

void walidacja_float (float *input);

void walidacja_float (float *input)
{

    float ZAKRES_START = 0;
    float ZAKRES_STOP = 69;

    while( true )
    {
        cin >> *input;
        if( cin.fail() )
        {
            cout << "Blednie wprowadzona wartosc. Sproboj ponownie: " << endl;

            cin.clear();
            cin.sync();
        } else
        {
            if( *input >= ZAKRES_START && *input <= ZAKRES_STOP )
                 break;
            else
                 cout << "Bledny zakres. Sproboj ponownie od "<<ZAKRES_START<<" do "<<ZAKRES_STOP<<": " << endl;
        }
    }
}



int main()
{
    float input;

    cout <<"Wprowadz input: ";
    walidacja_float(&input);



    cout << input;

    return 0;
}
