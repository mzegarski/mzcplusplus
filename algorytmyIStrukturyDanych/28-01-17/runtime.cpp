#include <iostream>
#include <fstream>

using namespace std;

int odczytaj (char nazwa[]);
void zapisz (char nazwa[], int x);
void wypisz (int x);


int main()
{
	int licznik;
	
	licznik = odczytaj("licznik.txt");
	licznik++;
	zapisz("licznik.txt", licznik);
	wypisz(licznik);
	
	return 0;
}

int odczytaj (char nazwa[])
{
	int pomoc;
	ifstream plik;
	plik.open(nazwa);
		if(!plik.fail())
		{
		plik >> pomoc;
		plik.close();
		}
	else pomoc = 0;
	return pomoc;
}

void zapisz (char nazwa[], int x)
{
	ofstream plik;
	plik.open(nazwa);
	plik << x;
	plik.close();	
}

void wypisz (int x)
{
	cout << "Uruchomienie programu nr: " << x;
}
