#include <iostream>
using namespace std;

void opis();
int wczytaj();
int mnozenie(int x, int y);
void wypisz (int x);

int main ()
{
	int a, b, wynik;
	
	opis();
	a = wczytaj();
	b = wczytaj();
	wynik = mnozenie(a, b);
	wypisz (wynik);
	
	return 0;	
}

void opis()
{
	cout <<"Program mnozy dwie liczby calkowite." << endl;
}

int wczytaj ()
{
	int x;
	cout <<"Podaj liczbe calkowita: ";
	cin >> x;
	return x;
}

int mnozenie (int x, int y)
{
	int wynik_mnozenia;
	wynik_mnozenia = x * y;
	return wynik_mnozenia;	
}

void wypisz (int x)
{
	cout <<"Wynik mnozenia to: " << x;
}

