#include <iostream>
using namespace std;

//program wczytuje tylko poprawna date z okesu 1901-2099
int main()
  {
  struct typ_daty
    {
    int rok;
    int miesiac;
    int dzien;
    } data;
  int dniwmc;
  
  cout << "Program wczytuje date z okresu 1901-2099." << endl;
  cout << "Podaj date" << endl;
  
  //rok
  do
    {
    cout <<  "Podaj rok     ";
    cin >> data.rok;
    if(data.rok<1901 || data.rok>2099) 
      cout<<"Musisz podac rok z zakresu 1901-2099"<<endl;  
    } while(data.rok<1901 || data.rok >2099);
  //miesiac
  do
    {
    cout <<  "Podaj miesiac ";
    cin >> data.miesiac;
    if(data.miesiac<1 || data.miesiac>12) 
      cout<<"Rok nie ma tylu miesiecy"<<endl;  
    } while(data.miesiac<1 || data.miesiac>12);
  //ustalenie liczby dni w miesiacu
  //...najczesciej jest 31
  dniwmc=31;     
  //...czasami 30
  if(data.miesiac==4 || data.miesiac==6 || data.miesiac==9 || data.miesiac==11) 
    dniwmc=30;
  //...i jest jeszcze luty i lata przestepne
  if(data.miesiac==2) 
    if(data.rok%4==0) dniwmc=29; else dniwmc=28;
  //ta prosta regula nie dziala np. dla lat 1900 i 2100
  //dzien
  do
    {
    cout <<  "Podaj dzien   ";
    cin >> data.dzien;
    if(data.dzien<1 || data.dzien>dniwmc) 
      cout<<"Ten miesiac nie ma tylu dni"<<endl;  
    } while(data.dzien<1 || data.dzien>dniwmc);
    
  cout << endl << "Podales date: " 
       << data.rok << "-" << data.miesiac << "-" << data.dzien << endl;
       
  return 0;
  }
