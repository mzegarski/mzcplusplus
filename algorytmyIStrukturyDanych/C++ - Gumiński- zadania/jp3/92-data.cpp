#include <iostream>
using namespace std;

//program wczytuje date - lacznie z niepoprawnymi 20xx-11-31 20xx-2-30 itp.
int main()
  {
  struct typ_daty
    {
    int rok;
    int miesiac;
    int dzien;
    } data;
  
  cout << "Program wczytuje date z okresu 1901-2099." << endl;
  cout << "Podaj date" << endl;
  
  //rok
  do
    {
    cout <<  "Podaj rok     ";
    cin >> data.rok;
    if(data.rok<1901 || data.rok>2099) 
      cout<<"Musisz podac rok z zakresu 1901-2099"<<endl;  
    } while(data.rok<1901 || data.rok >2099);
  //miesiac
  do
    {
    cout <<  "Podaj miesiac ";
    cin >> data.miesiac;
    if(data.miesiac<1 || data.miesiac>12) 
      cout<<"Rok nie ma tylu miesiecy"<<endl;  
    } while(data.miesiac<1 || data.miesiac>12);
  //dzien - tu brakuje wlasciwego ograniczenia liczby dni w miesiacu
  do
    {
    cout <<  "Podaj dzien   ";
    cin >> data.dzien;
    if(data.dzien<1 || data.dzien>31) 
      cout<<"Zaden miesiac nie ma tylu dni"<<endl;  
    } while(data.dzien<1 || data.dzien>31);
    
  cout << endl << "Podales date: " 
       << data.rok << "-" << data.miesiac << "-" << data.dzien << endl;
       
  return 0;
  }
