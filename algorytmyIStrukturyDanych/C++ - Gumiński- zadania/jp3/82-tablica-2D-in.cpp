//wczytywanie elementow tablicy dwuwymiarowej
#include <iostream>
#define N 3         //rozmiar tablicy
using namespace std;

int main()
  {
  int tablica[N][N];
  int w,k,l;
    
  //wczytywanie tablicy
  cout << "Podaj wartosci elementow tablicy wierszami" << endl;
  for(w=0;w<N;w++)
    for(k=0;k<N;k++)
      {
      cout << " X[" << w << ", " << k << "] = ";
      cin >> tablica[w][k];
      }
  cout << endl;
      
  //wypisanie tablicy
  for(w=0;w<N;w++)
    {
    for(k=0;k<N;k++) cout << tablica[w][k] << '\t';
    cout << endl;
    }
    
  return 0;
  }

