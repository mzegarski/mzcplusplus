#include <iostream> 
#include <cstdlib>  
#include <ctime>    
//rozmiar tablicy
#define N 10
using namespace std;

//program demostruje podstawowe operacje przeszukiwania tablicy

//PROTOTYPY FUNKCJI
//losowe wypelnienie tablicy liczbami mniejszymi od 1000
void wypelnij(int t[], int n);
//wypisanie liczb z tablicy
void wypisz(int t[], int n);
//najmniejsza liczba w tablicy  
int minimum(int t[], int n);
//najwieksza liczba w tablicy
int maksimum(int t[], int n);
//liczba liczb nieparzystych
int nieparzyste(int t[], int n);
//liczba liczb parzystych
int parzyste(int t[], int n);
//suma liczb w tablicy
int suma(int t[], int n);
//suma liczb parzystych w tablicy
int suma_parzystych(int t[], int n);
//suma liczb nieparzystych w tablicy
int suma_nieparzystych(int t[], int n);


int main()
  {
  int tab[N];
  
  srand(time(NULL));
  wypelnij(tab, N);
  wypisz(tab, N);
  
  cout << "Najmniejsza liczba to         " << minimum(tab, N) << endl;
  cout << "Najwieksza liczba to          " << maksimum(tab, N) << endl;
  cout << "Liczba liczb nieparzystych to " << nieparzyste(tab, N) << endl;
  cout << "Liczba liczb parzystych to    " << parzyste(tab, N) << endl;
  cout << "Suma wszystkich liczb to      " << suma(tab, N) << endl;
  cout << "Suma liczb parzystych to      " << suma_parzystych(tab, N) << endl;
  cout << "Suma liczb nieparzystych to   " << suma_nieparzystych(tab, N) << endl;
  cout << "Srednia liczb to              " << (double)suma(tab, N)/N << endl;
  
  return 0;
  }
  
//losowe wypelnienie tablicy liczbami mniejszymi od 1000
void wypelnij(int t[], int n)
  {
  int i;
  for(i=0; i<n; i++) t[i]=rand() % 1000;
  }

//wypisanie liczb z tablicy
void wypisz(int t[], int n)
  {
  int i;
  for(i=0; i<n; i++) cout << t[i] << endl;
  }

//najmniejsza liczba w tablicy  
int minimum(int t[], int n)
  {
  int najmniejsza, i;
  najmniejsza=t[0];
  for(i=1; i<n; i++) if(t[i]<najmniejsza) najmniejsza=t[i];
  return najmniejsza;
  }

//najwieksza liczba w tablicy
int maksimum(int t[], int n)
  {
  int najwieksza, i;
  najwieksza=t[0];
  for(i=1; i<n; i++) if(t[i]>najwieksza) najwieksza=t[i];
  return najwieksza;
  }  

//liczba liczb nieparzystych
int nieparzyste(int t[], int n)
  {
  int i,liczba=0;
  for(i=0; i<n; i++) if(t[i]%2!=0) liczba++;
  return liczba;
  }

//liczba liczb parzystych
int parzyste(int t[], int n)
  {
  int i,liczba=0;
  for(i=0; i<n; i++) if(t[i]%2==0) liczba++;
  return liczba;
  }

//suma liczb w tablicy
int suma(int t[], int n)
  {
  int i, liczba=0;
  for(i=0; i<n; i++) liczba+=t[i];
  return liczba;
  }
  
//suma liczb parzystych w tablicy
int suma_parzystych(int t[], int n)
  {
  int i, liczba=0;
  for(i=0; i<n; i++) if(t[i]%2==0) liczba+=t[i];
  return liczba;
  }
  
//suma liczb nieparzystych w tablicy
int suma_nieparzystych(int t[], int n)
  {
  int i, liczba=0;
  for(i=0; i<n; i++) if(t[i]%2!=0) liczba+=t[i];
  return liczba;
  }
    
