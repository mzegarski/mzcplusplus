//program demonstruje operacje na tablicy dwuwymiarowej
#include <iostream>
#include <cstdlib>
#include <ctime>
//rozmiar tablicy
#define N 10
using namespace std;

void wypisz(int t[N][N]);                 //wypisuje zawartosc tablicy
void wypelnienie_slimakowe(int t[N][N]);  //wypelnia tablice kolejnymi liczbami

int main()
  {
  //tablica NxN
  int tab[N][N];
  
  cout << endl << "Wypelnienie slimakowe" << endl;
  wypelnienie_slimakowe(tab);
  wypisz(tab);
  
  return 0;
  }

void wypisz(int t[N][N])
  {
  int w,k;
  for(w=0;w<N;w++) 
    {
    for(k=0;k<N;k++) cout << t[w][k] << '\t';
    cout << endl;
    }
  }

void wypelnienie_slimakowe(int t[N][N])
  {
  int w,k;       //indeks wiersza i kolumny
  int dw=0,dk=1; //kierunek ruchu
  int nr=2;      //numer po ktorym nastepuje skret
  int x=1;       //przyrost nr
  int liczba=1;  //liczba do wpisania (licznik krokow)
  k=N/2-1;
  w=N/2-1;
  while(liczba<=N*N)
    {
    t[w][k]=liczba++;
    w+=dw;
    k+=dk;
    if(liczba>=nr)
      {
      //nastepny zakret? (sprobuj samodzielnie utworzyc tak dzialajaca regule)
      if (nr==x*x+1) nr=x*x+x+1;
      else if (nr==x*x+x+1)nr=++x*x+1;
      //kolejnosc skretow 
      if(dk==1) {dw=1; dk=0;}
      else if(dw==1) {dw=0; dk=-1;}
      else if(dk==-1) {dw=-1; dk=0;}
      else {dw=0; dk=1;} 
      }
    }
  }


