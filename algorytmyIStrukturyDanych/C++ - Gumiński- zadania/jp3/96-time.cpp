#include <iostream>
#include <ctime>    //time, localtime, clock
using namespace std;

//Program wypisuje biezaca date i czas
int main()
  {
  tm *czas;                 //wskazanie na strukture daty-czasu
  /*
  struct tm
    {
	int	tm_sec;		// Seconds: 0-59 (K&R says 0-61?) 
	int	tm_min;		// Minutes: 0-59 
	int	tm_hour;	// Hours since midnight: 0-23 
	int	tm_mday;	// Day of the month: 1-31 
	int	tm_mon;		// Months *since* january: 0-11 
	int	tm_year;	// Years since 1900 
	int	tm_wday;	// Days since Sunday (0-6) 
	int	tm_yday;	// Days since Jan. 1: 0-365 
	int	tm_isdst;	// +1 Daylight Savings Time, 0 No DST
    };
  */
  time_t t;                 //typ czasu
  clock_t stop;             //typ czasu dzialania

  t=time(NULL);             //odczyt biezacego czasu
  czas=localtime(&t);       //zamiana na czas lokalny
    
  //dzien tygodnia
  switch(czas->tm_wday)
    {
    case 0: cout << "niedziela "; break;
    case 1: cout << "poniedzialek "; break;
    case 2: cout << "wtowtorek "; break;
    case 3: cout << "sroda "; break;
    case 4: cout << "czwartek "; break;
    case 5: cout << "piatek "; break;
    case 6: cout << "sobota "; break;
    }
  //data
  cout << czas->tm_mday << '-';
  cout << czas->tm_mon+1 << '-';
  cout << czas->tm_year+1900 << " ";
  //czas
  cout << czas->tm_hour << ':' ;
  cout << czas->tm_min << ':' ;
  cout << czas->tm_sec << endl;
    
  //czas dzialania
  stop=clock();
  cout << endl << "Program dzialal " ;
  cout << 1.0*stop/CLOCKS_PER_SEC;
  cout << " sekund" << endl;
  
  return 0;
  } 
  
