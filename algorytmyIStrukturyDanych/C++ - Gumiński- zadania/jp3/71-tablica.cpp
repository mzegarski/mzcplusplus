#include <iostream> 
#include <cstdlib>  //rand(), srand()
#include <ctime>    //time()
//rozmiar tablicy
#define N 10
using namespace std;

//program wypelnia tablice losowymi liczbami
int main()
  {
  int tab[N];
  int i;
  
  srand(time(NULL));
  //losowe wypelnienie tablicy liczbami mniejszymi od 1000
  for(i=0; i<N; i++) tab[i]=rand() % 1000;
  
  //wypisanie liczb z tablicy
  for(i=0; i<N; i++) cout << tab[i] << endl;
  
  return 0;
  }
