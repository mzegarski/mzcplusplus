#include <iostream> 
#include <cstdlib>  //rand() i srand()
#include <ctime>    //time()
//rozmiar tablicy
#define N 10
using namespace std;

//program oblicza srednia arytmetyczna liczb w tablicy
int main()
  {
  int tab[N];
  int i;
  int suma;
  double srednia;
  
  srand(time(NULL));
  //losowe wypelnienie tablicy liczbami mniejszymi od 1000
  for(i=0; i<N; i++) tab[i]=rand() % 1000;
  
  //wypisanie liczb z tablicy
  for(i=0; i<N; i++) cout << tab[i] << endl;
  
  //suma
  suma=0;
  for(i=1; i<N; i++) suma+=tab[i];
  cout << "Suma liczb to    " << suma << endl;
  
  //srednia
  srednia=(double)suma/N;
  cout << "Srednia liczb to " << srednia << endl;

  return 0;
  }
