//suma liczb w wierszach i w kolumnach 
#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;
#define N 3

int main()
  {
  int tablica[N][N];
  int w,k,suma;
  
  //losowe wypelnienie tablicy
  srand(time(NULL));
  for(w=0;w<N;w++)
    for(k=0;k<N;k++)
      tablica[w][k]=rand()%10;
      
  //wypisanie tablicy
  for(w=0;w<N;w++)
    {
    for(k=0;k<N;k++) cout << tablica[w][k] << '\t';
    cout << endl;
    }
  cout << endl;  
  
  //suma liczb w wierszach
  for(w=0;w<N;w++)
    {
    suma=0;
    for(k=0;k<N;k++) suma+=tablica[w][k];
    cout << "Suma liczb w wierszu " << w+1 << " wynosi " << suma << endl;
    }
  cout << endl;
    
  //suma liczb w kolumnach
  for(k=0;k<N;k++)
    {
    suma=0;
    for(w=0;w<N;w++) suma+=tablica[w][k];
    cout << "Suma liczb w kolumnie " << k+1 << " wynosi " << suma << endl;
    }

  return 0;
  }

