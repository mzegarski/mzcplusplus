#include <iostream>
using namespace std;

//program wczytuje tylko poprawna date z okesu 1753-
int main(void)
  {
  struct typ_daty 
    {
    int rok;
    int miesiac;
    int dzien;
    } data;
  int dniwmc;
  
  cout << "Program wczytuje tylko poprawna date od 1753 r." << endl;
  cout << "Podaj date" << endl;
  
  //rok
  do
    {
    cout <<  "Podaj rok     ";
    cin >> data.rok;
    if(data.rok<1753) 
      cout<<"Musisz podac rok od 1753r. "<<endl;  
    } while(data.rok<1753);
  //miesiac
  do
    {
    cout <<  "Podaj miesiac ";
    cin >> data.miesiac;
    if(data.miesiac<1 || data.miesiac>12) 
      cout<<"Rok nie ma tylu miesiecy"<<endl;  
    } while(data.miesiac<1 || data.miesiac>12);
  //ustalenie liczby dni w miesiacu
  //...najczesciej jest 31
  dniwmc=31;     
  //...czasami 30
  if(data.miesiac==4 || data.miesiac==6 || data.miesiac==9 || data.miesiac==11) 
    dniwmc=30;
  //...i jest jeszcze luty i lata przestepne
  if(data.miesiac==2) 
    if(data.rok%4==0 && data.rok%100!=0 || data.rok%400==0) dniwmc=29; 
    else dniwmc=28;
  //dzien
  do
    {
    cout <<  "Podaj dzien   ";
    cin >> data.dzien;
    if(data.dzien<1 || data.dzien>dniwmc) 
      cout<<"Ten miesiac nie ma tylu dni"<<endl;  
    } while(data.dzien<1 || data.dzien>dniwmc);
    
  cout << endl << "Podales date: " 
       << data.rok << "-" << data.miesiac << "-" << data.dzien << endl;
       
  return 0;
  }
