#include <iostream> 
#include <cstdlib>  
#include <ctime>    
//rozmiar tablicy
#define N 100
using namespace std;

//program demostruje podstawowe operacje przeszukiwania tablicy
int main()
  {
  int tab[N];
  int i;
  int najmniejsza, najwieksza, nieparzyste, dwucyfrowe;
  
  srand(time(NULL));
  //losowe wypelnienie tablicy liczbami mniejszymi od 1000
  for(i=0; i<N; i++) tab[i]=rand() % 1000;
  
  //wypisanie liczb z tablicy rodzielone tabulatorami
  for(i=0; i<N; i++) cout << tab[i] << '\t';
  cout << endl;
  
  //najmniejsza
  najmniejsza=tab[0];
  for(i=1; i<N; i++) if(tab[i]<najmniejsza) najmniejsza=tab[i];
  cout << "Najmniejsza liczba to " << najmniejsza << endl;
  
  //najwieksza
  najwieksza=tab[0];
  for(i=1; i<N; i++) if(tab[i]>najwieksza) najwieksza=tab[i];
  cout << "Najwieksza liczba to  " << najwieksza << endl;
  
  //liczba liczb nieparzystych
  nieparzyste=0;
  for(i=0; i<N; i++) if(tab[i]%2==1) nieparzyste++;
  cout << "Liczba liczb nieparzystych to " << nieparzyste << endl;
  
  //liczba liczb dwucyfrowych
  dwucyfrowe=0;
  for(i=0; i<N; i++) if(tab[i]>=10 && tab[i]<=99) dwucyfrowe++;
  cout << "Liczba liczb dwucyfrowych to  " << dwucyfrowe << endl;
  
  return 0;
  }
