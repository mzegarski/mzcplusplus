//suma liczb w wierszach i w kolumnach 
#include <iostream>
#include <cstdlib>
#include <ctime>
#include <conio.c>
using namespace std;
#define N 3

int main(void)
  {
  int tablica[N][N];
  int w,k,suma;
  
  //losowe wypelnienie tablicy
  srand(time(NULL));
  for(w=0;w<N;w++)
    for(k=0;k<N;k++)
      tablica[w][k]=rand()%10;
      
  //wypisanie tablicy i sum wierszy
  for(w=0;w<N;w++)
    {
    suma=0;
    for(k=0;k<N;k++) 
      {
      cout << tablica[w][k] << '\t';
      suma+=tablica[w][k];
      }
    textcolor(LIGHTGREEN);
    cout << suma << endl;
    textcolor(LIGHTGRAY);
    }
  //suma kolumn  
  textcolor(LIGHTCYAN);
  for(k=0;k<N;k++)
    {
    suma=0;
    for(w=0;w<N;w++) suma+=tablica[w][k];
    cout << suma << '\t';
    }
  textcolor(LIGHTGRAY);
  //suma wszystkich
  textcolor(LIGHTRED);
  suma=0;
  for(k=0;k<N;k++) for(w=0;w<N;w++) suma+=tablica[w][k];
  cout << suma << endl;
  textcolor(LIGHTGRAY);

  //suma liczb na glownej przekatnej
  suma=0;
  for(k=0;k<N;k++) suma+=tablica[k][k];
  cout << "Suma liczb na glownej przekatnej wynosi " << suma << endl;

  cout << endl << "Nacisnij klawisz aby zakonczyc" << endl;  
  getch();      
  return 0;
  }

