//program demonstruje operacje na tablicy dwuwymiarowej
#include <iostream>
#include <cstdlib>
#include <ctime>
//rozmiar tablicy
#define N 4
using namespace std;

void wypisz(int t[N][N]);                 //wypisuje zawartosc tablicy
void wypelnienie_losowe(int t[N][N]);     //wypelnia tablice losowo
void wypelnienie_wierszami(int t[N][N]);  //wypelnia tablice kolejnymi liczbami
void wypelnienie_kolumnami(int t[N][N]);
int suma_przekatnej(int t[N][N]);         //suma liczb na przekatnej
int suma_pod_przekatna(int t[N][N]);        
int suma_nad_przekatna(int t[N][N]);

int main()
  {
  //tablica NxN
  int tab[N][N];
  
  cout << endl << "Wypelnienie wierszami" << endl;
  wypelnienie_wierszami(tab);
  wypisz(tab);
  
  cout << endl << "Wypelnienie kolumnami" << endl;
  wypelnienie_kolumnami(tab);
  wypisz(tab);
    
  cout << endl << "Wypelnienie losowe" << endl;
  wypelnienie_losowe(tab);
  wypisz(tab);
 
  cout << endl << "Suma liczb na glownej przekatnej jest rowna " <<  suma_przekatnej(tab);
  cout << endl << "Suma liczb pod glowna przekatna jest rowna  " <<  suma_pod_przekatna(tab);
  cout << endl << "Suma liczb nad glowna przekatna jest rowna  " <<  suma_nad_przekatna(tab);

  return 0;
  }

void wypisz(int t[N][N])
  {
  int w,k;
  for(w=0;w<N;w++) 
    {
    for(k=0;k<N;k++) cout << t[w][k] << '\t';
    cout << endl;
    }
  }

void wypelnienie_losowe(int t[N][N])
  {
  int w,k;
  srand(time(NULL));
  for(w=0;w<N;w++) for(k=0;k<N;k++) t[w][k]=rand()%100;
  }

void wypelnienie_wierszami(int t[N][N])
  {
  int w,k;
  int liczba=1;
  for(w=0;w<N;w++) for(k=0;k<N;k++) t[w][k]=liczba++;
  }

void wypelnienie_kolumnami(int t[N][N])
  {
  int w,k;
  int liczba=1;
  for(k=0;k<N;k++) for(w=0;w<N;w++) t[w][k]=liczba++;
  }

int suma_przekatnej(int t[N][N])
  {
  int i;
  int suma=0;
  for(i=0;i<N;i++) suma+=t[i][i]; 
  //elementy na gl. przekatnej maja taki sam indeks wiersza i kolumny
  return suma;
  }

int suma_pod_przekatna(int t[N][N])
  {
  int w,k;
  int suma=0;
  for(w=0;w<N;w++) for(k=0;k<N;k++) if(k<w) suma+=t[w][k]; 
  //elementy pod gl. przekatnej maja mniejszy indeks kolumny
  return suma;
  }

int suma_nad_przekatna(int t[N][N])
  {
  int w,k;
  int suma=0;
  for(w=0;w<N;w++) for(k=0;k<N;k++) if(k>w) suma+=t[w][k]; 
  //elementy pod gl. przekatnej maja wiekszy indeks kolumny
  return suma;
  }
  
