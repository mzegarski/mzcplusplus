#include <iostream>
using namespace std;

//Program wypisuje piramide z gwiazdek
int main()
  {
  int w, k;     //wiersz kolumna
  int liczba;   //liczba gwiazdek
  
  cout << "Program wypisuje piramide z gwiazdek " << endl << endl;
  cout << "Podaj liczbe gwiazdek ";
  cin >> liczba;
  
  for(w=1; w<=liczba; w++)
    {
    for(k=1; k<=liczba-w; k++) cout << " ";
    for(k=1; k<=2*w-1; k++) cout << "*";
    cout << endl;
    }
  cout << endl;

  return 0;
  }
  
