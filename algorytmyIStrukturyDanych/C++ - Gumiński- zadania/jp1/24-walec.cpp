//program oblicza pojemnosc i powierzchnie walca
#include <iostream>
#include <cmath>
//#define M_PI 3.14159265
using namespace std;

int main()
  {
  double r, h, objetosc, pole;
  
  //wczytanie danych
  cout << "Program oblicza powierzchnie i objetosc walca" << endl;
  cout << "Podaj promien podstawy ";
  cin >> r;
  cout << "Podaj wysokosc stozka  ";
  cin >> h;
  //obliczenia
  pole=2*M_PI*r*r+2*M_PI*r*h;
  objetosc=M_PI*r*r*h;
  //wyniki
  cout << "Pole powierzchni walca wynosi " << pole << endl;
  cout << "Objetosc walca wynosi         " << objetosc << endl;
  
  return 0;
  }
