//program oblicza pojemnosc i powierzchnie stozka
#include <iostream>
#include <cmath>
//#define M_PI 3.14159265
using namespace std;

int main()
  {
  double r, h, objetosc, pole;
  
  //wczytanie danych
  cout << "Program oblicza powierzchnie i objetosc stozka" << endl;
  cout << "Podaj promien podstawy ";
  cin >> r;
  cout << "Podaj wysokosc stozka  ";
  cin >> h;
  //obliczenia
  pole=M_PI*r*r+M_PI*r*sqrt(r*r+h*h);
  objetosc=1.0/3*M_PI*r*r*h;
  //wyniki
  cout << "Pole powierzchni stozka wynosi " << pole << endl;
  cout << "Objetosc stozka wynosi         " << objetosc << endl;
  
  return 0;
  }
