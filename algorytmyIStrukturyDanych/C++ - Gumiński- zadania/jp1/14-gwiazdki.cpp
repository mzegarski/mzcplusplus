//program, ktory wypisuje na ekranie figury z gwiazdek
#include <iostream> 
using namespace std;

int main()
  {
  //prostokat
  cout << "****" << endl;
  cout << "****" << endl;
  cout << "****" << endl;
  cout << "****" << endl;
  //trojkat
  cout << "*" << endl;
  cout << "**" << endl;
  cout << "***" << endl;
  cout << "****" << endl;
  //piramida
  cout << "   *   " << endl;
  cout << "  ***  " << endl;
  cout << " ***** " << endl;
  cout << "*******" << endl;

  return 0;
  }

