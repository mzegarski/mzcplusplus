//program rozrozniajacy liczby dodatnie i ujemne oraz parzyste i nieparzyste
#include <iostream>
using namespace std;

int main()
  {
  int liczba;
  
  cout << "Program testuje dodatnosc i parzystosc liczby calkowitej" << endl;
  cout << "Podaj liczbe ";
  cin >> liczba;

  cout << "liczba ";
  if (liczba==0) cout << "zero ";
  else
    {
    if (liczba%2==0) cout << "parzysta ";
    else cout << "nieparzysta ";
    if (liczba>0) cout << "dodatnia ";
    else cout << "ujemna ";
    }
    
  return 0;
  }

