//program z zabezpieczeniem przed dzieleniem przez zero 
//(dodatek do kalkulatora program 26-kalkulator.cpp)
#include <iostream>
using namespace std;

int main()
  {
  int a,b;
  int iloraz,reszta;
  
  cout << "Program dzieli dwie liczby calkowite" << endl;
  cout << "Podaj pierwsza liczbe ";
  cin >> a;
  cout << "Podaj druga liczbe    ";
  cin >> b;
  
  //zmien ten fragment w programie 26-kalkulator.cpp
  if (b==0) cout << "Nie mozna dzielic przez zero!" << endl;
  else
    {
    iloraz=a/b;
    reszta=a%b;
    cout << a << " : " << b << " = " << iloraz << " reszty " << reszta << endl; 
    }
    
  return 0;
  }

