//program oblicza pojemnosc i powierzchnie kuli
#include <iostream>
#include <cmath>
//#define M_PI 3.14159265
using namespace std;

int main()
  {
  double r, objetosc, pole;
  
  //wczytanie danych
  cout << "Program oblicza powierzchnie i objetosc kuli" << endl;
  cout << "Podaj promien kuli ";
  cin >> r;
  //obliczenia
  pole=4.0*M_PI*r*r;
  objetosc=4.0/3*M_PI*r*r*r;
  //wyniki
  cout << "Pole powierzchni kuli wynosi " << pole << endl;
  cout << "Objetosc kuli wynosi         " << objetosc << endl;
  
  return 0;
  }
