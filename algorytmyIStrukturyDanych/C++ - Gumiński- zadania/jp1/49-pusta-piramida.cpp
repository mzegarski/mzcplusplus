#include <iostream>
using namespace std;

//Program wypisuje pusta piramide z gwiazdek
int main()
  {
  int w, k;     //wiersz kolumna
  int liczba;   //liczba gwiazdek
  
  cout << "Program wypisuje pusta piramide z gwiazdek " << endl << endl;
  cout << "Podaj liczbe gwiazdek ";
  cin >> liczba;
  
  for(k=1; k<=liczba-1; k++) cout << " ";
  cout << "*" << endl;
  for(w=2; w<=liczba-1; w++)
    {
    for(k=1; k<=liczba-w; k++) cout << " ";
    cout << "*";
    for(k=1; k<=2*w-3; k++) cout << " ";
    cout << "*";
    cout << endl;
    }
  for(k=1; k<=2*liczba-1; k++) cout << "*";
  cout << endl;

  return 0;
  }
  
