//prosty kalkulator
#include <iostream>
using namespace std;

int main()
  {
  int a, b; //argumenty
  int suma, roznica, iloczyn, iloraz, reszta;
  
  //wczytanie danych
  cout << "Prosty kalkulator dla liczb calkowitych" << endl;
  cout << "Podaj pierwsza liczbe ";
  cin >> a;
  cout << "Podaj druga liczbe    ";
  cin >> b;
  //obliczenia
  suma=a+b;
  roznica=a-b;
  iloczyn=a*b;
  iloraz=a/b; //zobacz tez zabezpieczenia program 33-dziel-zero.cpp
  reszta=a%b; //jw.
  //wyniki
  cout << endl;
  cout << a << " + " << b << " = " << suma << endl;
  cout << a << " - " << b << " = " << roznica << endl;
  cout << a << " * " << b << " = " << iloczyn << endl;
  cout << a << " : " << b << " = " << iloraz << " reszty " << reszta << endl;
  
  return 0;
  }
