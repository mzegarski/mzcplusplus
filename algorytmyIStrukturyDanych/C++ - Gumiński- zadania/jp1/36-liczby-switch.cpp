//zamiana liczby na zapis slowny szkic -9 do 9
#include <iostream>
using namespace std;

int main()
  {
  int liczba;
  
  cout << "Program zamienia jednocyfrowa liczbe na zapis slowny" << endl;
  cout << "Podaj liczbe ";
  cin >> liczba;
  
  if (liczba==0) cout << "zero" << endl;
  else
    {
    if (liczba<0)
      {
      cout << "minus ";
      liczba=-liczba;
      }
    switch (liczba) 
      {
      case 1: cout << "jeden "; break;
      case 2: cout << "dwa "; break;
      case 3: cout << "trzy "; break;
      case 4: cout << "cztery "; break;
      case 5: cout << "piec "; break;
      case 6: cout << "szesc "; break;
      case 7: cout << "siedem "; break;
      case 8: cout << "osiem "; break;
      case 9: cout << "dziewiec "; break;
      default: cout<< "liczba poza zakresem";
      }
    }

  return 0;
  }
  
