//pierwiastki trojmianu kwadratowego
#include <iostream>
#include <cmath>    
using namespace std;

int main()
  {
  double a,b,c;  //wspolczynniki trojmianu
  double delta;  //delta
  double x1,x2;  //wyniki

  //opis  
  cout << "Program oblicza pierwiastki trojmianu kwadratowego postaci" << endl;
  cout << "ax^2 + bx + c = 0" << endl;

  //wczytanie danych
  cout << "Podaj a = ";
  cin >> a;
  cout << "Podaj b = ";
  cin >> b;
  cout << "Podaj c = ";
  cin >> c;

  if (a==0) cout << "To nie jest trojmian" << endl;
  else
    {
    //obliczenia wstepne
    delta=b*b-4*a*c;
    //decyzje, obliczenia i wypisanie wynikow
    if (delta<0) cout << "Brak rozwiazan" << endl;
    if (delta==0) 
      {
      x1=-b/(2*a);
      cout << "Jedeno rozwiazanie" << endl;
      cout << "x = " << x1 << endl;
      }
    if (delta>0)
      {
      x1=(-b-sqrt(delta))/(2*a);
      x2=(-b+sqrt(delta))/(2*a);
      cout << "Dwa rozwiazania" << endl;
      cout << "x1 = " << x1 << endl;
      cout << "x2 = " << x2 << endl;
      }
    }

  return 0;
  }

