//zamiana liczby na zapis slowny szkic -9 do 9
#include <iostream>
using namespace std;

int main()
  {
  int liczba;
  
  cout << "Program zamienia jednocyfrowa liczbe na zapis slowny" << endl;
  cout << "Podaj liczbe ";
  cin >> liczba;
  
  if (liczba==0) cout << "zero" << endl;
  else
    {
    if (liczba<0)
      {
      cout << "minus ";
      liczba=-liczba;
      }
    if (liczba==1) cout << "jeden ";
    else if (liczba==2) cout << "dwa ";
    else if (liczba==3) cout << "trzy ";
    else if (liczba==4) cout << "cztery ";
    else if (liczba==5) cout << "piec ";
    else if (liczba==6) cout << "szesc ";
    else if (liczba==7) cout << "siedem ";
    else if (liczba==8) cout << "osiem ";
    else if (liczba==9) cout << "dziewiec ";
    else cout << "liczba poza zakresem ";
    }

  return 0;
  }
  
