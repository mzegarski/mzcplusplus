#include <iostream>
using namespace std;

//Program wypisuje litery parami ab bc cd .. xy yz
int main()
  {
  char znak, drugi;
  
  cout << "Program wypisuje litery parami ab bc cd .. xy yz " << endl << endl;
  
  for(znak='a'; znak<='y'; znak++) 
    {
    drugi=znak+1;
    cout << znak << drugi << " ";
    }
  cout << endl;
    
  for(znak='z'; znak>='b'; znak--) 
    {
    drugi=znak-1;
    cout << znak << drugi << " ";
    }
  cout << endl;

  return 0;
  }
  
