//program, ktory wypisuje na ekranie liczby i wartosci wyrazen
#include <iostream> 
using namespace std;

int main()
  {
  cout << "10" << endl;  //napis diesiec
  cout << 10 << endl;    //liczba dziesiec
  cout << 010 << endl;   //liczba osiem (zapis oktalny-osemkowy)

  cout << "2+2" << endl; //napis
  cout << 2+2 << endl;   //wartosc wyrazenia
  
  cout << 60000*60000 << endl;   // -694967296 przekroczenie zaktesu typu int
 
  cout << 5/2 << endl;   //2 dzielenie liczb calkowitych 
   
  return 0;
  }

