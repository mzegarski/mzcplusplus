//program wyswietlajacy sume liczb
#include <iostream>
using namespace std;

int main()
  {
  int a,b,suma;
  
  //wczytanie danych
  cout << "Program oblicza sume dwoch liczb calkowitych" << endl;
  cout << "Podaj pierwsza liczbe  ";
  cin >> a;
  cout << "Podaj druga liczbe     ";
  cin >> b;
  //obliczenia
  suma=a+b;
  //wyniki
  cout << "Suma tych liczb wynosi " << suma << endl;
  
  return 0;
  }
