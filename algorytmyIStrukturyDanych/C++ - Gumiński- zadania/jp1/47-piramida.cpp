#include <iostream>
using namespace std;

//Program wypisuje piramide z gwiazdek
int main()
  {
  int w, k;     //wiersz kolumna
  int liczba;   //liczba gwiazdek
  int sp;       //liczba spacji w wierszu
  int gw;       //liczba gwiazdek w wierszu
  
  cout << "Program wypisuje piramide z gwiazdek " << endl << endl;
  cout << "Podaj liczbe gwiazdek ";
  cin >> liczba;
  
  //wartosci dla pierwszego wiersza
  sp=liczba-1;
  gw=1;
  
  for(w=1; w<=liczba; w++)
    {
    for(k=1; k<=sp; k++) cout << " ";
    for(k=1; k<=gw; k++) cout << "*";
    cout << endl;
    sp=sp-1;
    gw=gw+2;
    }
  cout << endl;

  return 0;
  }
  
