#include <iostream>
#include <iomanip>
//niestandardowa biblioteka C do sterowania kolorem, pozycja i klawiatura konsoli
#include "conio.h"
using namespace std;

int main()
  {
  char znak;
  int kod;
  cout << "TABLICA KODOW ZNAKOW" << endl;
  for(kod=' '; kod<=255; kod++)
    {
    znak=kod;
    cout << "   " << znak;
    cout << setw(4) << kod;
    }
  cout << endl;
  
  cout << "Przycisnij ENTER aby kontynuowac";
  do{}while(getch()!=13); //ENTER kod 13
  
  cout << "KODY KLAWISZY getch()" << endl;
  cout << "Przyciskaj klawisze i ich kombinacje - ESC konczy" << endl;
  do
    {
    kod=getch(); 
    znak=kod;
    cout << znak << " " << kod << endl;
    }while(kod!=27);
  
  return 0;
  }

