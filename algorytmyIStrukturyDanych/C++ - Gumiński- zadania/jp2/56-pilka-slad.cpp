#include <iostream>
#include <cstdlib>
//niestandardowa biblioteka C do sterowania kolorem, pozycja i klawiatura konsoli
#include "conio.c"
using namespace std;

//odbijajaca sie pilka zostawiajaca kolorowy slad
int main()
  {
  //srodek ekranu
  int x=40, y=12;
  //w prawo w dol
  int dx=1, dy=1;
  
  do
    {
    textcolor(rand()%16);     //losowy kolor znakow
    gotoxy(x,y);
    cout << "O";
    _sleep(20);
    gotoxy(x,y);
    cout << ".";              //zostaw slad
    x+=dx;
    y+=dy;
    //odbicia od krawedzi
    if(x>=80)dx=-1;
    if(x<=1) dx=1;
    if(y>=24)dy=-1;
    if(y<=1) dy=1;
    }while(!kbhit());         //dowolny klawisz konczy
  
  return 0;
  }
