#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;

//zabawa w za duzo za malo - odgadnij liczbe ktora pomyslal komputer
int main()
  {
  int liczba;       //liczba losowa
  int licznik=0;    //licznik numeru pytania
  int odp;          //odpowiedz czlowieka (1..1000)
  
  srand(time(NULL));
  liczba=rand()%1000+1;

  cout << "Odgadnij liczbe od 1 do 1000" << endl;
  
  do 
    {
    licznik=licznik+1;
    cout << licznik << " Podaj liczbe ";
    do 
      {
      cin >> odp;
      }while(odp<1 || odp>1000);
    if(odp<liczba) cout << "   Za mala" << endl;
    if(odp>liczba) cout << "   Za duza" << endl;
    } while(odp!=liczba);

  cout << endl << "Zgadles! to liczba " << liczba << endl;
  cout << "Zgadywales " << licznik << " razy" << endl;
    
  return 0;
  }
  
  
