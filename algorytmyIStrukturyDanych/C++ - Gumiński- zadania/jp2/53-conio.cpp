#include <iostream>
//niestandardowa biblioteka C do sterowania kolorem, pozycja i klawiatura konsoli
#include "conio.c"

using namespace std;

int main()
  {
  textbackground(BLUE);     //niebieskie tlo
  clrscr();                 //czyszczenie ekranu
  textcolor(YELLOW);        //zolte znaki
  gotoxy(34,12);            //ustalenie pozycji na ekranie
  cout << "SRODEK EKRANU";
  
  gotoxy(1,20);
  for(int j=0; j<3; j++)
    for(int i=0; i<16; i++)
      {
      textbackground(i);
      cout << "     ";
      }
  
  gotoxy(1,25);
  textcolor(LIGHTRED);
  cout << "Przycisnij ESC aby zakonczyc";
  do{}while(getch()!='\033'); //ESC to znak o kodzie 27 = 033
  
  return 0;
  }

