#include <iostream>
#include <cstdlib> //_sleep
//niestandardowa biblioteka C do sterowania kolorem, pozycja i klawiatura konsoli
#include "conio.c"
using namespace std;

//pilka odbijajaca sie od krawedzi ekranu
int main()
  {
  int x,y;
  int dx,dy;
  //srodek ekranu
  x=40;
  y=13;
  //w prawo w dol
  dx=1;
  dy=1;
  
  do
    {
    gotoxy(x,y);
    cout << "O";
    _sleep(20);
    gotoxy(x,y);
    cout << " ";
    x+=dx;
    y+=dy;
    //odbicia od krawedzi
    if(x>=80)dx=-1;
    if(x<=1) dx=1;
    if(y>=24)dy=-1;
    if(y<=1) dy=1;
    }while(!kbhit()); //dowolny klawisz konczy
  
  return 0;
  }
