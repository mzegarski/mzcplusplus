#include <iostream>
#include <cstdlib>
#include <cstring>
//niestandardowa biblioteka C do sterowania kolorem, pozycja i klawiatura konsoli
#include "conio.c"
using namespace std;

//animowany napis odbijajacy sie od krawedzi ekranu
int main()
  {
  int x=1;
  int dx=1;
  char napis[] = "..:: S U P E R    P R O G R A M ::..";
  int len = strlen(napis);
  
  do
    {
    gotoxy(x,1);
    cout << napis;
    _sleep(100);
    gotoxy(x,1);
    cout << ' ';
    gotoxy(x+len,1);
    cout << ' ';
    x+=dx;
    if(x>=80-len)dx=-1;
    if(x<=1)dx=1;
    }while(!kbhit());
  
  return 0;
  }
