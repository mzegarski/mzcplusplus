#include <iostream>
//niestandardowa biblioteka C do sterowania kolorem, pozycja i klawiatura konsoli
#include "conio.c"
using namespace std;

//krzyzyk przesuwany po ekranie klawiszami kursora
int main()
  {
  //srodek ekranu
  int x=40, y=12;
  unsigned char kl;
  
  do
    {
    gotoxy(x,y);
    cout << "X";
    kl=getch();
    gotoxy(x,y);
    cout << " ";
    if(kl==0 || kl==224) //klawisze specjalne (2 kody)
      {
      kl=getch();
      if(kl==72)y--;    //gora
      if(kl==80)y++;    //dol
      if(kl==75)x--;    //lewo
      if(kl==77)x++;    //prawo
      }
    //ograniczenia
    if(x<=1) x=1;
    if(x>=80)x=80;
    if(y<=1) y=1;
    if(y>=24)y=24;
    }while(kl!=27);     //ESC konczy
  
  return 0;
  }
