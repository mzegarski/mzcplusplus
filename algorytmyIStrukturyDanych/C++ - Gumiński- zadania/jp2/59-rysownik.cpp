#include <iostream>
//niestandardowa biblioteka C do sterowania kolorem, pozycja i klawiatura konsoli
#include "conio.c"
using namespace std;

//mini program do rysowania
int main()
  {
  //srodek ekranu
  int x=40, y=12;
  unsigned char kl;
  
  //opis
  cout << "P R O G R A M   R Y S O W N I K" << endl;
  cout << "uzywaj klawiszy kursora do rysowania," << endl;
  cout << "klawiszy funkcyjnych F1 do F10 do zmiany kolorow." << endl;
  cout << "ESC konczy zabawe" << endl;
  cout << endl << "Nacisnij dowolny klawisz" << endl;
  getch();
  clrscr();
  textbackground(LIGHTGRAY);
  do
    {
    gotoxy(x,y);
    cout << "X";
    kl=getch();
    gotoxy(x,y);
    cout << " ";
    if(kl==0 || kl==224) //klawisz funkcyjny (dwa kody)
      {
      kl=getch();
      if(kl==72)y--;    //gora
      if(kl==80)y++;    //dow
      if(kl==75)x--;    //lewo
      if(kl==77)x++;    //prawo
      if(kl>=59 && kl <=68) textbackground(kl-43); //F1-F10 kolory
      }
    if(kl=='C' || kl=='c') clrscr(); 
    //ograniczenia
    if(x<=1)x=1;
    if(x>=80)x=80;
    if(y<=1)y=1;
    if(y>=24)y=24;
    }while(kl!=27);     //ESC konczy
  textbackground(BLACK);
  
  return 0;
  }
