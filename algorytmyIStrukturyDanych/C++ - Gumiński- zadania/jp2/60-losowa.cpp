#include <iostream> //cout
#include <cstdlib>  //rand, srand
#include <ctime>    //time
using namespace std;

//liczby losowe z podanego zakresu
int main()
  {
  int liczba;       //liczba losowa
  int i;            //zmienna pomocnicza 
  
  //wystartowanie generatora liczb losowych
  srand(time(NULL));
  
  //liczby losowe
  cout << "Liczby losowe" << endl;
  for(i=0;i<10;i++)
    {
    liczba=rand();
    cout << liczba << endl;
    }

  //liczby losowe trzycyfrowe
  cout << "Liczby losowe trzycyfrowe" << endl;
  for(i=0;i<10;i++)
    {
    liczba=100+rand()%900;  //100-najmniejsza trzycyfrowa 900-tyle jest liczb trzycyfrowych
    cout << liczba << endl;
    }
    
  return 0;
  }
  
  
