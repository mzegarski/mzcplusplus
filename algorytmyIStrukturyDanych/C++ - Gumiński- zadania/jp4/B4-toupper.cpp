#include <iostream>
#include <cstring> 
#include <cctype>  
using namespace std;
#define MAX 80

//program zamienia wszystkie pierwsze znaki wyrazow na duze litery
int main()
  {
  char napis[MAX];
  int k;
  
  cout << "Podaj napis ";
  cin.getline(napis,MAX);
  
  //zamiana pierwszego znaku na duza litere
  napis[0]=toupper(napis[0]);
  //zamiana kazdego znaku po spacji na duzy
  for(k=1; k<strlen(napis); k++)
    if(napis[k-1]==' ') napis[k]=toupper(napis[k]);
    
  //wypisanie napisu
  cout << "Napis: " << napis << endl;

  return 0;
  }
