#include <iostream>
#include <string>
#include <vector>
using namespace std;

//vector jako dynamiczna tablica
int main()
  {
  int liczba;
  vector<int> t;
  
  do 
    {
    cout << "Podaj liczbe (zero konczy) ";
    cin >> liczba;  
    t.push_back(liczba);    //dodajemy na koncu wektora
    }while(liczba!=0);
  t.pop_back();             //usuwamy ostatnio wprowadzone zero
  
  //wypisanie liczb
  cout << "Podales " << t.size() << " liczb" << endl;
  cout << "Podane liczby to: " << endl;  
  for(int i=0; i<t.size(); ++i)
      cout << t[i] << endl;
  
  return 0;
  }


