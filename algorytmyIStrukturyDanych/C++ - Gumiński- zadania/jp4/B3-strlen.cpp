#include <iostream>
#include <conio.h>
#include <cstring> //dla strlen()
#include <cctype>  //dla toupper()
using namespace std;
#define MAX 80

int main()
  {
  char napis[MAX];
  int k;
  
  cout << "Program zamienia pierwszy znak napisu na duzy" << endl;
  cout << "Podaj napis: ";
  cin.getline(napis,MAX);
  
  //zamiana pierwszego znaku na duza litere
  if(napis[0]>='a' && napis[0]<='z') napis[0]=napis[0]-'a'+'A';
  //to samo realizuje funkcja toupper()
  //napis[0]=toupper(napis[0]);
  
  //wypisanie napisu
  cout << "Napis: " << napis << endl;

  //liczymy znaki do wystapienia kodu zero
  k=0;
  while(napis[k]!=0) k++;
  //to samo realizuje funkcja strlen()
  //k=strlen(napis);
  cout << "Napis zawiera " << k << " znakow" << endl;
  
  return 0;
  }
