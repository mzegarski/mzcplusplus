#include <iostream>
#include <cstring> 
#include <cctype>  
using namespace std;
#define MAX 80

//program zamienia pierwsze znaki wyrazow na duze litery, a pozostale na male
void pierwsze_duze_inne_male(char *napis)
  {
  int i;
  //zamiana pierwszego znaku na duza litere
  napis[0]=toupper(napis[0]);
  //zamiana kazdego znaku po spacji na duzy
  for(int i=1; i<strlen(napis); i++)
    if(!isalnum(napis[i-1])) napis[i]=toupper(napis[i]);
    else napis[i]=tolower(napis[i]);
  }
  
int main()
  {
  char napis[MAX];
  int k;
  
  cout << "Program zamienia pierwsze znaki wyrazow na duze litery, a pozostale na male" << endl;
  cout << "Podaj napis: ";
  cin.getline(napis,MAX);
  
  pierwsze_duze_inne_male(napis);
  
  cout << "Napis: " << napis << endl;

  return 0;
  }
