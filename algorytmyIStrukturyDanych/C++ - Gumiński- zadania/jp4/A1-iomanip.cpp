#include <iostream>
#include <iomanip>  //setw, setfill, setprecision
#include <cstdlib>
#include <ctime>
using namespace std;
#define N 8

int main()
  {
  int liczba;
  double x;
  
  srand(time(NULL));
  for(int i=0; i<N; ++i)
    {
    liczba=rand();
    cout << setfill(' ') 
         << setw(8) << liczba << "  " 
         << setfill('0') 
         << setw(6) << liczba << "  "
         << setw(4) << hex << liczba << dec << endl;
    }
  cout << setfill(' ') << endl;
  
  for(int i=0; i<N; ++i)
    {
    x=(double)rand()/RAND_MAX;
    cout << setw(8) << setprecision(1) << x 
         << setw(8) << setprecision(3) << x << endl;
    }
  cout << endl;
  
  return 0;
  } 
  
