#include <iostream>
using namespace std;

int czytaj_liczbe(int min, int max);    //prototyp funkcji

int main()
  {
  int liczba;
  
  liczba = czytaj_liczbe(1,1000);
  cout << "Podana liczba to " << liczba << endl;
  
  return 0;
  } 

//definicja funkcji
int czytaj_liczbe(const int min, const int max)
  {
  int x;
  
  do 
    {
    //kontrola czy poprzedni odczyt zakonczyl sie poprawnie
    if(cin.fail())
      {
      cin.clear();            //wyczyszczenie informacji o bledzie
      cin.ignore(1000,'\n');  //wyczyszczenie bufora wejsciowego 
      }                        //pomin do 1000 znakow az napotkasz znak nowej linii
    cout << "Podaj liczbe calkowita od " << min << " do " << max << " ";
    cin >> x;
    }while(cin.fail() || x<min || x>max);    
  
  return x;
  }  
  
