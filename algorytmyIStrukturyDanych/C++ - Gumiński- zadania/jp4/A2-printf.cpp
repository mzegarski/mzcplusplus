#include <cstdio>   //printf
#include <cstdlib>
#include <ctime>
using namespace std;
#define N 8

int main()
  {
  int liczba;
  double x;
  
  srand(time(NULL));
  for(int i=0; i<N; ++i)
    {
    liczba=rand();
    printf("%8d  %06d  %04x\n", liczba, liczba, liczba);
    }
  printf("\n");
  
  for(int i=0; i<N; ++i)
    {
    x=(double)rand()/RAND_MAX;
    printf("%8.1f%8.3f \n", x, x);
    }
  
  return 0;
  } 
  
