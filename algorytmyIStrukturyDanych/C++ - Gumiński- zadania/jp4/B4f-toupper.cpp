#include <iostream>
#include <cstring> 
#include <cctype>  
using namespace std;
#define MAX 80

//program zamienia wszystkie pierwsze znaki wyrazow na duze litery
void pierwsze_duze(char *napis)
  {
  int i;
  //zamiana pierwszego znaku na duza litere
  napis[0]=toupper(napis[0]);
  //zamiana kazdego znaku po spacji na duzy
  for(int i=1; i<strlen(napis); i++)
    if(napis[i-1]==' ') napis[i]=toupper(napis[i]);
  }
  
int main()
  {
  char napis[MAX];
  int k;
  
  cout << "Program zamienia wszystkie pierwsze znaki wyrazow na kapitaliki" << endl;
  cout << "Podaj napis ";
  cin.getline(napis,MAX);
  
  pierwsze_duze(napis);
  
  cout << "Napis: " << napis << endl;

  return 0;
  }
