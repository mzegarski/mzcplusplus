#include <iostream>
#include <cstring>
using namespace std;
#define MAX 80

//program odwraca kolejnosc znakow w napisie
int main()
  {
  char napis[MAX];
  char *nap;       //zmienna dynamiczna
  char *p, *k;     //wskazania
  char znak;
  
  cout << "Program wypisuje napis wspak" << endl;
  cout << "Podaj napis: ";
  cin.getline(napis,MAX);
  //nowy napis
  nap=new char[strlen(napis)+1];
  //kopiowanie zawartosci
  strcpy(nap, napis);
  //wypisanie napisu
  cout << "Podany napis: " << nap << endl;
  //zamiana znakow wspak
  p=&nap[0]; 
  k=&nap[strlen(nap)-1];
  while(p<k) 
    {
    znak=*p;
    *p=*k;
    *k=znak;
    p++;
    k--;
    }
  //wypisanie napisu
  cout << "Napis wspak: " << nap << endl;
  //zwolnienie pamieci
  delete[] nap;

  return 0;
  }
