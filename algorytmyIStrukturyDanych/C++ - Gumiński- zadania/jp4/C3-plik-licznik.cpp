#include <iostream>
#include <fstream>
using namespace std;

int czytaj_licznik(char nazwa[])
  {
  int licz=0;
  ifstream we;
  we.open(nazwa);
  if (we.is_open())
    {
    we >> licz;
    we.close();
    }
  return licz;
  }
  
void zapisz_licznik(char nazwa[], int x)
  {
  ofstream wy;
  wy.open(nazwa);
  if(wy.is_open())
    {
    wy << x;
    wy.close();
    }
  }
  
int main()
  {
  int licznik;
  char nazwa[] = "licznik.txt";
  
  licznik=czytaj_licznik(nazwa);
  licznik++;
  zapisz_licznik(nazwa, licznik);
  
  cout << "Uruchomienie programu numer " << licznik << endl;
  
  return 0;
  }
