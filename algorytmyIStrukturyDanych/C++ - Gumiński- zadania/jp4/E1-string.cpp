#include <iostream>
#include <string>
#include <climits>
using namespace std;

int main()
  {
  string wyraz, napis;
  
  cout << "Podaj wyraz ";
  //cin >> wczyta do spacji
  cin >> wyraz;      
  cout << "Podales wyraz " << wyraz << endl;
  cout << "Wyraz ma dlugosc " << wyraz.length() << " znakow " << endl;
  cout << endl;
  //ignorujemy ewentualne znaki za pierwszym wyrazem
  cin.ignore(INT_MAX,'\n');

  cout << "Podaj napis ";
  //getline wczyta do entera
  getline(cin, napis);  
  cout << "Podales wyraz " << napis << endl;
  cout << "Wyraz ma dlugosc " << napis.length() << " znakow " << endl;
  cout << endl;
  
  //przeciazony operator +
  napis=wyraz+" i "+napis;
  cout << "Suma napisow = " << napis << endl;
  
  return 0;
  }


