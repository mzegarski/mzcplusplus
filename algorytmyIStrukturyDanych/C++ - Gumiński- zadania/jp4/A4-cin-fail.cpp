#include <iostream>
using namespace std;

int main()
  {
  int liczba;
  
  do 
    {
    //kontrola czy poprzedni odczyt zakonczyl sie niepoprawnie
    if(cin.fail())
      {
      cin.clear();            //wyczyszczenie informacji o bledzie
      cin.ignore(1000,'\n');  //wyczyszczenie bufora wejsciowego 
      }                       //pomin do 1000 znakow az napotkasz znak nowej linii
    cout << "Podaj liczbe calkowita od 1 do 1000 ";
    cin >> liczba;
    }while(cin.fail() || liczba<1 || liczba>1000);    
  cout << "Podana liczba to " << liczba << endl;
  
  return 0;
  } 
  
