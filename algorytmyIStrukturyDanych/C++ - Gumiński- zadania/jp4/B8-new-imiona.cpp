#include <iostream>
#include <cstring>  //strcmp, strcpy
using namespace std;
#define MAX 80
#define N 20

//program zapamietuje kolejno wpisywane imiona
int main()
  {
  char napis[MAX];
  char* osoby[N];
  int i, k=0;
  
  do
    {
    cout << "Podaj imie (koniec-konczy) ";
    cin.getline(napis, MAX);
    //utworzenie nowego napisu
    osoby[k]=new char[strlen(napis)+1];
    //zapamietanie imienia jesli rozne od 'koniec'
    if(strcmp(napis,"koniec")!=0)
      {
      strcpy(osoby[k], napis);
      k++;
      }
    //wypisanie imion
    for(i=0; i<k; i++) cout << i+1 << ". " << osoby[i] << endl;
    }while(k<N && strcmp(napis,"koniec")); //koniec gdy 'koniec' lub N-imion 
    //zwolnienie pamieci
    for(i=0; i<k; i++) delete[] osoby[i];

  return 0;
  }
