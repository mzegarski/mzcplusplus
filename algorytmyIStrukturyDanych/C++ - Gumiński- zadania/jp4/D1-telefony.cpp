#include <iostream>
#include <fstream>
#include <climits>
using namespace std;
#define MAX 12
#define MAX_OPIS 40


struct wpis
  {
  char opis[MAX_OPIS];
  int tel;
  };

//odczyt wpisu z klawiatury  
void dodaj(wpis &w);
//wypisanie wszystkich wpisow
void pokaz(wpis w[], int n);
//zapamietanie wpisow na dysku
void zapisz(char nazwa[], wpis w[], int n);
//odczyt wpisow z dysku
void odczytaj(char nazwa[], wpis w[], int &n);

int main()
  {
  char nazwa[] = "notes.txt";
  wpis notes[MAX];
  int liczba=0;
  int wybor;
  //czytamy z dysku poprzednia zawartosc
  odczytaj(nazwa, notes, liczba);
  do
    {
    cout << endl;
    cout << " 1. Dodaj wpis " << endl;
    cout << " 2. Pokaz wpisy " << endl;
    cout << " 3. Koniec " << endl;
    cout << " Twoj wybor: ";
    cin >> wybor;
    if(cin.fail()) { cin.clear(); cin.ignore(INT_MAX,'\n'); }
    switch(wybor)
      {
      case 1:
        {
        if(liczba>MAX-1) 
          {
          cout << "KONIEC PAMIECI " << endl;
          break;
          }
        dodaj(notes[liczba]); 
        liczba++;
        break;
        }
      case 2:
        {
        pokaz(notes, liczba);
        break;
        }
      }
    }while(wybor!=3);
    //zapisujemy na dysk zmiany
    zapisz(nazwa, notes, liczba);
    
  return 0;
  }
  
void dodaj(wpis &w)
  {
  cout << " Podaj opis: ";
  cin >> w.opis;
  if(cin.fail()) { cin.clear(); cin.ignore(INT_MAX,'\n'); }
  cout << " Podaj tel.: ";
  cin >> w.tel;
  if(cin.fail()) { cin.clear(); cin.ignore(INT_MAX,'\n'); }
  }
  
void pokaz(wpis w[], int n)
  {
  int i;
  cout << "WPISY" << endl;
  for(i=0; i<n; i++)
    {
    cout << i+1 << ". " << w[i].opis << " \t" << w[i].tel << endl;
    }
  }

void zapisz(char nazwa[], wpis w[], int n)
  {
  int i;
  ofstream wy;
  wy.open(nazwa);
  if(wy.is_open())
    {
    for(i=0; i<n; i++)
      {
      //opis w pierwsze linii
      wy << w[i].opis << endl;
      //tel w drugiej 
      wy << w[i].tel << endl;
      }
    wy.close();
    }
  }
  
void odczytaj(char nazwa[], wpis w[], int &n)
  {
  ifstream we;
  n=0;
  we.open(nazwa);
  if(we.is_open())
  {
    while(!we.eof())
      {
      we >> w[n].opis;
      we >> w[n].tel;
      //jesli byl blad to konczymy odczyt
      if(we.fail()) break;
      n++;
      if(n>MAX-1) break;
      }
    we.close();
    }
  }
  
  
