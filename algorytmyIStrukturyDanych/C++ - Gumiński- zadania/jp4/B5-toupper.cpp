#include <iostream>
#include <cstring> 
#include <cctype>  
using namespace std;
#define MAX 80

//program zamienia pierwsze znaki wyrazow na duze litery, a pozostale na male
int main()
  {
  char napis[MAX];
  int k;
  
  cout << "Program zamienia pierwsze znaki wyrazow na duze litery, a pozostale na male" << endl;
  cout << "Podaj napis: ";
  cin.getline(napis,MAX);
  
  //zamiana pierwszego znaku na duza litere
  napis[0]=toupper(napis[0]);
  //zamiana kazdego znaku po spacji na duzy
  for(k=1; k<strlen(napis); k++)
    if(!isalnum(napis[k-1])) 
      napis[k]=toupper(napis[k]); //pierwsza litere wyrazu na duzy znak
    else 
      napis[k]=tolower(napis[k]); //kolejne litery wyrazu na maly znak
  
  //to samo bez p�tli for (przeanalizuj) 
  //napis[0]=toupper(napis[0]);
  //k=0;
  //while(napis[k]!=0)            //gdy kod litery rozny od zera
  //  if(!isalnum(napis[k++]))    //k++ PO! sprawdzeniu napis[k]
  //    napis[k]=toupper(napis[k]);
  //  else 
  //    napis[k]=tolower(napis[k]);

  //wypisanie napisu
  cout << "Napis: " << napis << endl;

  return 0;
  }
