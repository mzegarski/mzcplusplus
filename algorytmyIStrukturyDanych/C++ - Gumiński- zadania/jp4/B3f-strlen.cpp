#include <iostream>
using namespace std;
#define MAX 80

//dlugosc napisu - strlen()
int dlugosc(char napis[])
  {
  int len=0;
  while(napis[len]!=0) len++;
  return len;
  }
    
//zamiana znaku na duzy - toupper()
char kapitalik(char znak)
  {
  return znak>='a' && znak<='z' ? znak-'a'+'A' : znak;
  }

//zamiana znaku na maly - tolower()  
char wersalik(char znak)
  {
  return znak>='A' && znak<='Z' ? znak-'A'+'a' : znak;
  }
  
int main()
  {
  char napis[MAX];
  int k;
  
  cout << "Program zamienia pierwszy znak napisu na duzy" << endl;
  //wczytanie napisu
  cout << "Podaj napis: ";
  cin.getline(napis,MAX);
  
  //zamiana pierwszego znaku na duzy
  napis[0]=kapitalik(napis[0]);
  
  //wypisanie napisu
  cout << "Napis: " << napis << endl;

  //dlugosc napisu w znakach
  k=dlugosc(napis);
  cout << "Napis zawiera " << k << " znakow" << endl;
  
  return 0;
  }
