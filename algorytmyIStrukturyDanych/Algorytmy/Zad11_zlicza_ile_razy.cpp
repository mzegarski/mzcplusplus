//11. Zlicza ile razy wyst�puje ka�da z liter we wprowadzonym tek�cie
#include <iostream>
#include <conio.h>
#include <iomanip>

#define MAX_DLUGOSC 10000 //dlugosc bufora odczytu linii (jak dluga moze byc linia
#define ZNAKI_ASCII 256 //ilosc znakow ascii

using namespace std;

void czysc_i_wczytaj_linie(char *tablica_znakow);
void zlicz_znaki(int *tablica_sumy, char *tablica_znakow); // z tablicy znakow i zapisz w tablicy sumy
void wyswietl_wynik(int *tablica_sumy);

int main()
{
cout<<"Wprowadz dowolny tekst"<<endl;
int tablica_sumy[ZNAKI_ASCII]={0,}; // przechowuje ile razy wystapila literka
char tablica_znakow[MAX_DLUGOSC]; // przechowuje odczytana linie tekstu
do
{
czysc_i_wczytaj_linie(tablica_znakow);
zlicz_znaki(tablica_sumy, tablica_znakow);
}while(tablica_znakow[0]!=0); //powtarzaj tak dlugo jak odczytana linia tekstu nie jest pusta
wyswietl_wynik(tablica_sumy);
return 0;
}

void czysc_i_wczytaj_linie(char *tablica_znakow)
{
int licznik;
for(licznik=0;licznik<MAX_DLUGOSC;licznik++) //petla czysci cala tablice
{
tablica_znakow[licznik]=0;
}
getline(cin,MAX_DLUGOSC); // wpisujemy do tablicy nowa linie tekstu

//string wpisany_tekst;
//getline(cin, wpisany_tekst);



}

void zlicz_znaki(int *tablica_sumy, char *tablica_znakow)
{
char znak;
int licznik=0;
while(tablica_znakow[licznik]!=0) //tak dlugo az nie napotkamy pustego pola w tablicy
{
znak=tablica_znakow[licznik]; //odczytujemy znak z danego pola
tablica_sumy[znak]++; //zwiekszamy o 1 ilosc w polu o nr odpowiadajacym nr ascii znaku
licznik++;
}
}

void wyswietl_wynik(int *tablica_sumy) // wyswietlamy tablice w przedzialach zawierajacych sumy ilosci literek
{
int licznik;
char znak;
cout<<endl;
for(licznik='a';licznik<='z';licznik++){
znak = licznik;
cout<<znak<<" = "<<setw(6)<<tablica_sumy[licznik]<<" | ";
if(licznik%5==1) cout<<endl; //co 5 odpowiedz do nastepnej linii reszta 1 wynika z wartosci malych literek
}
cout<<endl<<endl;
for(licznik='A';licznik<='Z';licznik++){
znak = licznik;
cout<<znak<<" = "<<setw(6)<<tablica_sumy[licznik]<<" | ";
if(licznik%5==4) cout<<endl; //co 5 odpowiedz do nastepnej linii reszta 4 wynika zwartosci duzych literek
}
cout<<endl;
}
