/* Przykladowe kolo z CPP
** Mozecie wykorzystac moj kod pod warunkiem zmiany nazwy zmiennych na swoje i wyrzucenia komentarzy
** tlumaczacych dzialanie programu
** Krzysiek
*/

#include <iostream>
#include <fstream>
#include <cstdlib>
using namespace std;


//funkcja wczytująca dane z pliku tekstowego
//zwraca wskaznik na tablice dynamiczna
//przyjmuje jako parametr nazwe pliku oraz adres zmiennej do ktorej zapiszemy rozmiar
int* wczytajDane(char nazwaPliku[30], int &r) {
	ifstream plik;
	plik.open(nazwaPliku);

	char wiersz[20];
	int rozmiar;
	int* tablica;
	int licznik = 0;

	if (plik.good()) { //sprawdzamy powodzenie otwarcia pliku, inaczej zwracamy blad (plik moze nie istniec)

        //pobieramy pierwszy wiersz pliku ktory jest rozmiarem tablicy
		plik >> wiersz;
		rozmiar = atoi(wiersz); //konwersja z tablicy znakow do inta (array to int)

        //utworzenie tablicy dynamicznej o pobranym rozmiarze
		tablica = new int[rozmiar];

        //pobieramy kolejne dane z pliku - elementy ktore zapisujemy do tablicy
		while(licznik < rozmiar) {
			plik >> wiersz;
			tablica[licznik] = atoi(wiersz);
			licznik++;
		}
	} else {
		cout << "Nie mozna otworzyc pliku" << endl;
	}

	r = rozmiar;
	plik.close();
	return tablica;
}

//funkcja zapisujaca do pliku raport o wolnym miejscu w pojemnikach i wyswietlajaca rezultat na ekranie
//przyjmuje wskaznik na tablice z pojemnikami, jej rozmiar oraz nazwe pliku do ktorego zapisze wyniki
//za kazdym razem utworzy plik na nowo lub jezeli istnieje to go nadpisze
void zapiszRaport(int* tablica, int rozmiar, char nazwaPliku[30]) {
    ofstream plik;
	plik.open(nazwaPliku);

    //oprocz zapisania wszystkiego do pliku wyswietlamy rezultaty w konsoli
    cout << endl << "Wolne miejsce w pojemnikach: " << endl;

	if (plik.good()) {
        plik << "Wolne miejsce w pojemnikach: " << endl;
        for (int i = 0; i < rozmiar; i++) {
            plik << "pojemnik " << i+1 << ": " << tablica[i] << endl;
            cout << "pojemnik " << i+1 << ": " << tablica[i] << endl;
        }
	} else {
	    cout << "Blad zapisania pliku raportu";
	}
	plik.close();
}

int main() {
	int ilePojemnikow; //te zmienne podajemy fukncji wczytujacej zeby do nich zapisac rozmiar
	int ileTowarow;
	int* pojemniki = wczytajDane("pojemniki.txt", ilePojemnikow); //przypisanie tablicy zwracanej przez funkcje do lokalnego wskaznika
	int* towary = wczytajDane("towary.txt", ileTowarow);
	bool umieszczono; //zmienna do ktorej bedziemy zapisywac powodzenie/niepowodzenie umieszczenia towaru w pojemnikach

    //wybieramy petla kolejno towar i druga petla szukamy dla niego odpowiedniego pojemnika
	for (int i = 0; i < ileTowarow; ++i) {
        umieszczono = false;
		for (int j = 0; j < ilePojemnikow; ++j) {
			if (towary[i] <= pojemniki[j]) {    //sprawdzamy czy rozmiar towaru miesci sie w pojemniku
				cout << "Towar " << i+1 << " - Pojemnik " << j+1 << endl; // indeks tablicy zaczyna sie od 0 a wyswietlamy numerek od jeden wiekszy tak jak na przykladzie
				pojemniki[j] -= towary[i]; //od rozmiaru pojemnika odejmujemy rozmiar towaru
				umieszczono = true; //umiescilismy towar w jednym z pojemnikow
				break; //umiescilismy towar w pojemniku, przerywamy petle zeby nie sprawdzac kolejnych pojemnikow
			}
		}
        //jezeli nie umiescilismy towaru w zadnym z pojemnikow to wyswietlamy komunikat
		if (!umieszczono) { // sprawdzamy wartosc false
            cout << "Towar " << i+1 << " nie miesci sie do pojemnikow" << endl;
            umieszczono = false; //przestawiamy z true na false
		}
	}

    zapiszRaport(pojemniki, ilePojemnikow, "raport.txt");
    system("pause");
	return 0;
}
