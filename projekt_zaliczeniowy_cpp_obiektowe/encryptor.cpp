/*
Projekt zaliczeniowy:
Szyfrowanie i deszyfrowanie tekstu
na podstawie prostego, symetrycznego klucza.

Funkcjonalno��:
1. Obs�ugiwanie plik�w za pomoc� klasy MyFile:
-otwieranie pliku,
-wczytywanie z pliku do stringa
-zapisywanie z stringa do pliku
-czyszczenie zawarto�ci pliku

2. Szyfrowanie/deszyfrowanie za pomoc� klasy Encryptor:
-szyfrowanie wskazanego stringa
-deszyfrowanie wskazanego stringa

TODO:
Mo�e jakie� proste GUI?

Maciej Zegarski
maciekzegarski@gmail.com
*/
#include <iostream>
#include <fstream>
#include <string>

using namespace std;

class MyFile
{
    string filename;

public:
    MyFile();
    ~MyFile() {};

};

MyFile::MyFile()
{

}


int main ()
{
    MyFile input;


    string full_text = "";
    string line;
    ifstream myfile ("input.txt");
    if (myfile.is_open())
    {
        while ( getline (myfile,line) )
        {
        full_text += line;
        full_text += "\n";
        }
        myfile.close();
    }

    cout << "Wczytany tekst z pliku: \n"<<full_text;

    return 0;
}
