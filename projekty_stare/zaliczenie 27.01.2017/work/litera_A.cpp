//Autor: Maciej Zegarski (indeks 11602)
//maciekzegarski@gmail.com

#include <iostream>
#include <stdlib.h>
#include <conio.h>
#include <string>
#include <sstream>

using namespace std;

//rozmiar litery
#define MIN 4
#define MAX 60

int main ()
{
    cout << "Program rysuje litere 'A'. Zaleca sie zmaksymilizowac okno." << endl << endl;

    /*--------------------------------------------------------------*/
    /*                          WALIDACJA                           */
    /*  by Maciej Zegarski (maciekzegarski@gmail.com)               */
    /*  KONWERTUJE STRINGA NA INT LUB FLOAT                         */
    /*   NIE POZWALA NA WPROWADZANIE ZNAK�W INNYCH NIZ CYFRY        */
    /*  (WYJATEK: JEDEN MINUS NA POCZATKU I JEDNA KROPKA)           */
    /*  OBSLUGUJE LICZBY UJEMNE                                     */
    /*  ZEZWALA NA "m" MIEJSC PO PRZECINKU                          */
    /*  ZWRACA KOMUNIKATY IDENTYFIKUJACE BLAD                       */
    /*--------------------------------------------------------------*/

    string input;
    int n, i;
    bool zakres;

     cout <<"Podaj rozmiar litery A (zakres od "<< MIN <<" do "<< MAX <<"). Zatwierdz klawiszem ENTER: ";

    do
    {
        n=0; //liczba wyjsciowa
        i=0; //licznik
        zakres == false; //zakres ustawia sie w ostatniej petli if

        do
        {
            getline(cin, input);
            if (input [0]==0) cout <<"Wykryto blednie uzyty ENTER. Sproboj ponownie: "; //return
            if (input [0]==48) cout <<"Wykryto zero. Sproboj ponownie: "; //zero
        }while (input [0]==0 || input [0]==48 );

       for (i; i<input.length(); i++)
        {
            if (input[i]<48 || input[i]>57) // 48 == 0, 57 == 9
            {
                cout <<"Wykryto znak ktory nie jest cyfra. Sproboj ponownie: ";
                    break;
            }
        }

        if ( i==input.length())
        {
            istringstream iss(input); //zamiana string na n
            iss >> n; //zamiana string na n

            if (n< MIN || n>MAX) //TUTAJ USTAL ZAKRES DLA N
            {
                cout <<"Wykryto bledny zakres (zakres od "<< MIN <<" do "<< MAX <<"). Sproboj ponownie: ";
                zakres = false;
            }
            else zakres = true;
        }

    }while(zakres==false || i!=input.length() ); //konczy gdy zakres sie zgadza i zostaly sprawdzone wszystkie znaki

    //RYSOWANIE LITERY A
    system("cls");
    unsigned char znak;
    do
    {

        cout << "Obecny rozmiar to: " << n <<". Klawisz '+' zwieksza, klawisz '-' zmniejsza. ESC zeby wyjsc" << endl << endl;

        //licznik wierszy
        int l=0;

        //czubek
        for (int i=0; i<n-1 ;i++) cout <<" ";
        cout <<"*"<<endl;

        //rysowanie do srodka
        for (l; l<(n-1)/2 ;l++)
        {
            for (int i=0; i<n-(l+2) ; i++) cout <<" ";
            cout <<"*";
            for (int i=0; i<l*2+1; i++) cout <<" ";
            cout <<"*";
            cout <<endl;
        }

        //srodek
        for (int i=0; i<n-(l+2) ; i++) cout <<" ";
        if (n%2==0) for (int i=0; i<n+1; i++) cout <<"*";
        else for (int i=0; i<n+2; i++) cout <<"*";
        cout <<endl;
        l++;

        //rysowanie po srodku
        for (l; l<n-1;l++)
        {
            for (int i=0; i<n-(l+2) ; i++) cout <<" ";
            cout <<"*";
            for (int i=0; i<l*2+1; i++) cout <<" ";
            cout <<"*";
            cout <<endl;
        }

        //zwiekszanie i zmniejszanie litery
        znak = getch();
        if (znak == 43) n++; //znak +
        else if (znak == 45) n--; // znak -
        system("cls");
        if (n>=MAX) n=MAX;
        if (n<=MIN) n=MIN;

    }while(znak!= 27); //ESC

    return 0;
}
