//Autor: Maciej Zegarski (indeks 11602)
//maciekzegarski@gmail.com

#include <iostream>
#include <stdlib.h>
#include <conio.h>
#include <string>
#include <sstream>

using namespace std;

int main ()
{
    //walidacja
    string input;
    long double n;
    int i, p, m, lk;
    bool zakres;

    cout << "Program wczytuje kwote w PLN i rozbija ja na minimalna liczbe banknotow i monet."<< endl << endl;
    cout << "Podaj kwote w formacie PLN (kropka oddziela grosze) do miliona: ";

    do
    {
        n=0; //liczba wyjsciowa
        i=0; //licznik
        m=2; //miejsca po kropce
        lk=0; //licznik kropek
        zakres == false; //zakres ustawia sie w ostatniej petli if

        do
        {
            p=1; //parzystosc liczby
            getline(cin, input);
            if (input[0]==45) //minus
            {
                input.erase(0, 1); //usuwanie pierwszego znaku
                p = -1;
            }
            if (input [0]==0) cout <<"Wykryto blednie uzyty ENTER. Sproboj ponownie: "; //return
            if (input [0]==48 && input[1]==48) cout <<"Wykryto blednie uzyte zero. Sproboj ponownie: "; //zero
            if (input [0]==46) cout <<"Wykryto brak cyfr przed kropka. Sproboj ponownie: "; //kropka
        }while (input [0]==0 || (input [0]==48 && input[1]==48) || input [0]==46);

       for (i; i<input.length(); i++)
        {
            if (input[i]==46) //kropka
            {
                lk++;
                if (lk>1)
                {
                    cout <<"Wykryto za duzo kropek. Sproboj ponownie: ";
                    break;
                }
                if ( !(input.length()-i-1 >0 && (input.length()-i-1) <=(m)) )
                {
                    cout <<"Wykryto bledna liczbe znakow po przecinku (max "<<m<<"). Sproboj ponownie: ";
                    break;
                }
                continue;
            }
            if (input[i]<48 || input[i]>57) // 48 == 0, 57 == 9
            {
                cout <<"Wykryto znak ktory nie jest cyfra. Sproboj ponownie: ";
                    break;
            }
        }

        if ( i==input.length())
        {
            istringstream iss(input); //zamiana string na n
            iss >> n; //zamiana string na n

            n = n*p; //zmiana znaku jezeli pierwszy znak byl minus
            if (n<=0 || n>=1000000.01) //TUTAJ USTAL ZAKRES DLA N
            {
                cout <<"Wykryto bledny zakres (reszte mozna uzyskac dla zakresu od 0.01 do miliona). Sproboj ponownie: ";
                zakres = false;
            }
            else zakres = true;
        }

    }while(zakres==false || i!=input.length() ); //konczy gdy zakres sie zgadza i zostaly sprawdzone wszystkie znaki

    //liczenie reszty
    system("cls");
    long double kwota;
    unsigned char znak;
    float tab_banknoty[14] = {20000, 10000, 5000, 2000, 1000, 500, 200, 100, 50, 20, 10, 5, 2, 1}; //1000000000000

    do
    {
        kwota = n*100;

        int tab_wynik[14] = {0};
        char litera = 'a';


        for (int i=0; i < 14 ; i++)
        {
        while ((kwota - tab_banknoty[i])>=0)
            {
            kwota = kwota -(tab_banknoty[i]);
            tab_wynik [i]++;
            }
        }

    //wypisywanie
    cout <<"Banknoty i monety: "<< endl;
    for (int i = 0; i< 14; i++)
    {
        cout <<"["<<litera<<"]"<<'\t';
        litera++;
        if (tab_banknoty[i]==1000000000000)cout<<"OFF ";
        else cout << (tab_banknoty[i]/100);
        cout <<": "<<'\t'<<tab_wynik[i]<< endl;
    }

    //obsluga klawiszy
    cout <<endl<< "Klawisz przy nominale wlacza/wylacza liczenie. Reset: z."<< endl <<"Nacisnij ESC by zakonczyc.";
    znak = getch();
    if (znak == 97) //a
    {
        if (tab_banknoty [0] == 20000) tab_banknoty [0] = 1000000000000;
        else tab_banknoty [0] = 20000;
    }
    if (znak == 98) //b
    {
        if (tab_banknoty [1] == 10000) tab_banknoty [1] = 1000000000000;
        else tab_banknoty [1] = 10000;
    }
    if (znak == 99) //c
    {
        if (tab_banknoty [2] == 5000) tab_banknoty [2] = 1000000000000;
        else tab_banknoty [2] = 5000;
    }
    if (znak == 100) //d
    {
        if (tab_banknoty [3] == 2000) tab_banknoty [3] = 1000000000000;
        else tab_banknoty [3] = 2000;
    }
    if (znak == 101) //e
    {
        if (tab_banknoty [4] == 1000) tab_banknoty [4] = 1000000000000;
        else tab_banknoty [4] = 1000;
    }
    if (znak == 102) //f
    {
        if (tab_banknoty [5] == 500) tab_banknoty [5] = 1000000000000;
        else tab_banknoty [5] = 500;
    }
    if (znak == 103) //g
    {
        if (tab_banknoty [6] == 200) tab_banknoty [6] = 1000000000000;
        else tab_banknoty [6] = 200;
    }
    if (znak == 104) //h
    {
        if (tab_banknoty [7] == 100) tab_banknoty [7] = 1000000000000;
        else tab_banknoty [7] = 100;
    }
    if (znak == 105) //i
    {
        if (tab_banknoty [8] == 50) tab_banknoty [8] = 1000000000000;
        else tab_banknoty [8] = 50;
    }
    if (znak == 106) //j
    {
        if (tab_banknoty [9] == 20) tab_banknoty [9] = 1000000000000;
        else tab_banknoty [9] = 20;
    }
    if (znak == 107) //k
    {
        if (tab_banknoty [10] == 10) tab_banknoty [10] = 1000000000000;
        else tab_banknoty [10] = 10;
    }
    if (znak == 108) //l
    {
        if (tab_banknoty [11] == 5) tab_banknoty [11] = 1000000000000;
        else tab_banknoty [11] = 5;
    }
    if (znak == 109) //m
    {
        if (tab_banknoty [12] == 2) tab_banknoty [12] = 1000000000000;
        else tab_banknoty [12] = 2;
    }
    if (znak == 110) //n
    {
        if (tab_banknoty [13] == 1) tab_banknoty [13] = 1000000000000;
        else tab_banknoty [13] = 1;
    }
    if (znak == 122) //z
    {
        tab_banknoty[0] = 20000;
        tab_banknoty[1] =10000;
        tab_banknoty[2] =5000;
        tab_banknoty[3] =2000;
        tab_banknoty[4] =1000;
        tab_banknoty[5] =500;
        tab_banknoty[6] =200;
        tab_banknoty[7] =100;
        tab_banknoty[8] =50;
        tab_banknoty[9] =20;
        tab_banknoty[10] =10;
        tab_banknoty[11] =5;
        tab_banknoty[12] =2;
        tab_banknoty[13] =1;
    }
    system("cls");
    }while(znak!= 27);
    return 0;
}
