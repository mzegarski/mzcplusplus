//Autor: Maciej Zegarski (indeks 11602)
//maciekzegarski@gmail.com

#include <iostream>
#include <conio.h>
#include <string>
#include <sstream>
using namespace std;

int main ()
{




    //walidacja

    string input;
    long double n;
    int i, p, m, lk;
    bool zakres;

    cout << "Program wczytuje kwote w PLN i rozbija ja na minimalna liczbe banknotow i monet."<< endl << endl;
    cout << "Podaj kwote w formacie PLN (kropka oddziela grosze) do 99 miliardow (miliard to 9 zer): ";

    do
    {
        n=0; //liczba wyjsciowa
        i=0; //licznik
        m=2; //miejsca po kropce
        lk=0; //licznik kropek
        zakres == false; //zakres ustawia sie w ostatniej petli if

        do
        {
            p=1; //parzystosc liczby
            getline(cin, input);
            if (input[0]==45) //minus
            {
                input.erase(0, 1); //usuwanie pierwszego znaku
                p = -1;
            }
            if (input [0]==0) cout <<"Wykryto blednie uzyty ENTER. Sproboj ponownie: "; //return
            if (input [0]==48 && input[1]==48) cout <<"Wykryto blednie uzyte zero. Sproboj ponownie: "; //zero
            if (input [0]==46) cout <<"Wykryto brak cyfr przed kropka. Sproboj ponownie: "; //kropka
        }while (input [0]==0 || (input [0]==48 && input[1]==48) || input [0]==46);

       for (i; i<input.length(); i++)
        {
            if (input[i]==46) //kropka
            {
                lk++;
                if (lk>1)
                {
                    cout <<"Wykryto za duzo kropek. Sproboj ponownie: ";
                    break;
                }
                if ( !(input.length()-i-1 >0 && (input.length()-i-1) <=(m)) )
                {
                    cout <<"Wykryto bledna liczbe znakow po przecinku (max "<<m<<"). Sproboj ponownie: ";
                    break;
                }
                continue;
            }
            if (input[i]<48 || input[i]>57) // 48 == 0, 57 == 9
            {
                cout <<"Wykryto znak ktory nie jest cyfra. Sproboj ponownie: ";
                    break;
            }
        }

        if ( i==input.length())
        {
            istringstream iss(input); //zamiana string na n
            iss >> n; //zamiana string na n

            n = n*p; //zmiana znaku jezeli pierwszy znak byl minus
            if (n<=0 || n>=99999999999.99) //TUTAJ USTAL ZAKRES DLA N
            {
                cout <<"Wykryto bledny zakres (reszte mozna uzyskac dla zakresu od 0.01 do 99 miliardow). Sproboj ponownie: ";
                zakres = false;
            }
            else zakres = true;
        }

    }while(zakres==false || i!=input.length() ); //konczy gdy zakres sie zgadza i zostaly sprawdzone wszystkie znaki

//liczenie reszty

    long double kwota;
    float tab_banknoty[14] = {20000, 10000, 5000, 2000, 1000, 500, 200, 100, 50, 20, 10, 5, 2, 1}; //1000000000000
    int tab_wynik[14] = {0};

    kwota = n*100;
for (int i=0; i < 14 ; i++)
{


    while ((kwota - tab_banknoty[i])>=0)
    {
        kwota = kwota -(tab_banknoty[i]);
        tab_wynik [i]++;
    }
}

//wypisywanie
cout << endl <<"Banknoty i monety: "<< endl << endl;

for (int i = 0; i< 14; i++)
{
    cout << (tab_banknoty[i]/100)<<": "<<'\t'<<tab_wynik[i]<< endl;
}
    getch();
    return 0;
}
