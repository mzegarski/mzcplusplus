#include <iostream>
#include <conio.h>
#include <string>
#include <sstream>
using namespace std;

int main()
{


    /*--------------------------------------------------------------*/
    /*                          WALIDACJA                           */
    /*  by Maciej Zegarski (maciekzegarski@gmail.com)               */
    /*  KONWERTUJE STRINGA NA INT LUB FLOAT                         */
    /*   NIE POZWALA NA WPROWADZANIE ZNAK�W INNYCH NIZ CYFRY        */
    /*  (WYJ�TEK: JEDEN MINUS NA POCZATKU I JEDNA KROPKA)           */
    /*  OBSLUGUJE LICZBY UJEMNE                                     */
    /*  ZEZWALA NA "m" MIEJSC PO PRZECINKU                          */
    /*  ZWRACA KOMUNIKATY IDENTYFIKUJACE BLAD                       */
    /*--------------------------------------------------------------*/

    string input;
    int n;
    // float n; //wybierz int czy float
    int i, p, m, lk;
    bool zakres;

    cout << "Podaj input: ";

    do
    {
        n=0; //liczba wyjsciowa
        i=0; //licznik
        m=2; //miejsca po kropce
        lk=0; //licznik kropek
        zakres == false; //zakres ustawia sie w ostatniej petli if

        do
        {
            p=1; //parzystosc liczby
            getline(cin, input);
            if (input[0]==45) //minus
            {
                input.erase(0, 1); //usuwanie pierwszego znaku
                p = -1;
            }
            if (input [0]==0) cout <<"Wykryto blednie uzyty ENTER. Sproboj ponownie: "; //return
            else if (input [0]==48 && input[1]==48) cout <<"Wykryto blednie uzyte zero. Sproboj ponownie: "; //zero
            else if (input [0]==46) cout <<"Wykryto brak cyfr przed kropka. Sproboj ponownie: "; //kropka
            else if (input [0]==48 && !(input [1]==46)) cout <<"Wykryto blednie uzyte zero. Sproboj ponownie: "; //zero i nie kropka

        }while (input [0]==0 || (input [0]==48 && input[1]==48) || input [0]==46 || (input [0]==48 && !(input [1]==46)));

       for (i; i<input.length(); i++)
        {
            if (input[i]==46) //kropka
            {
                lk++;
                if (lk>1)
                {
                    cout <<"Wykryto za duzo kropek. Sproboj ponownie: ";
                    break;
                }
                if ( !(input.length()-i-1 >0 && (input.length()-i-1) <=(m)) )
                {
                    cout <<"Wykryto bledna liczbe znakow po przecinku (max "<<m<<"). Sproboj ponownie: ";
                    break;
                }
                continue;
            }
            if (input[i]<48 || input[i]>57) // 48 == 0, 57 == 9
            {
                cout <<"Wykryto znak ktory nie jest cyfra. Sproboj ponownie: ";
                    break;
            }
        }

        if ( i==input.length())
        {
            istringstream iss(input); //zamiana string na n
            iss >> n; //zamiana string na n

            n = n*p; //zmiana znaku jezeli pierwszy znak byl minus
            if (n==10) //TUTAJ USTAL ZAKRES DLA N
            {
                cout <<"Wykryto bledny zakres. Sproboj ponownie: ";
                zakres = false;
            }
            else zakres = true;
        }

    }while(zakres==false || i!=input.length() ); //konczy gdy zakres sie zgadza i zostaly sprawdzone wszystkie znaki

    cout << "Koniec! Liczba n to: "<< n << endl;

    return 0;
}
