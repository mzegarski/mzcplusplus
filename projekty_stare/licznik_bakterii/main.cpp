#include <iostream>

using namespace std;

int populacja=1, bakterie, godziny=0;

int main()
{
    cout << "Bakterie rozmnazaja sie 2 razy co godzine. Program sprawdza ile godzin musi minac zeby uzyskac dana ilosc bakterii." << endl;
    cout << "Ile chcesz otrzymac bakterii? ";
    cin >> bakterie;

    while(populacja <= bakterie)
    {
        godziny++;
        populacja = populacja *2;
        cout <<"Minelo godzin: " << godziny << " Liczba bakterii: "<< populacja << endl;
    }

    return 0;
}
