#include <iostream>

using namespace std;

int main()
{

    cout << "Podaj wysokosc choinki: ";
    int n;
    cin >> n;
    if (n==0){cout << "Zero."; return 0;}//konczenie programu dla wartosci zero
    cout << endl << endl;

     //pierwszy wiersz: "wierzcholek"
    for (int i=1;i<=n-1; i++) cout << " "; //spacje po lewej
    cout << "*" << endl; //wierzcholek, nowa linia

    int l=0; //licznik srodkowych wierszy
    for (l; l<n-2; l++)
    {
        for (int i=1;i<=(n-l-2); i++) cout << " "; //spacje po lewej
        cout << "*"; //pierwsza gwiazdka
        for (int i=1; i<=((l*2+1)); i++) cout << "."; //kropki w srodku
        cout << "*" << endl;
    }
    if (n!=1)
    {
        for (int i=1; i<=n*2-1;i++) cout <<"*"; //gwiazdki w podstawie
    }

    cout << endl;
    return 0;
}
