#include <iostream>
#include <fstream>

using namespace std;

//ilosc wczytywanych plikow
const int N = 5;

int main ()
{
    //przykladowy komentarz
    //program wczytuje z pliku linijke literek a potem zapisuje ja w innym pliku
    //korzysta z tablic dynamicznych

    //tworzenie zmiennej do obslugi plikow
    ifstream file;

    //wskazywanie dla zmiennej sciezki i pliku ktory ma otworzyc
    file.open ("plik.txt");

    //tworzenie dynamicznej tablicy o rozmiarze N na znaki (char)
    char*literki = new char [N];

    //wczytywanie kolejno znakow ktore nie sa w zaden sposob rozdzielone!
    file >> literki;

    //tworzenie dynamicznej tablicy o rozmairze N na liczby (int)
    int*liczby = new int [N];

    //wczytywanie po kolei N liczb do tablicy
    for (int i=0; i<N; i++)
    {
        file >> liczby[i];
    }
    //jak konczymy korzystac z danego pliku to go zamykamy
    file.close();

    //zapisywanie wyniku do pliku
    //tworzenie zmiennej do zapisywywania
    ofstream save;

    //wskazanie dla zmiennej sciezki i nazwy pliku
    save.open ("rezultat.txt");

    //zapisanie po kolei literek
    for (int i=0; i<N; i++)
    {
        save <<literki[i];
    }
    //zapisanie do pliku znaku nowej linii
    save<<endl;

    //wypisanie linia po linii liczb z opisem
    for (int i=0; i<N; i++)
    {
        save<<"Liczba numer "<<i+1<<": "<<liczby[i]<<endl;
    }
    //jak konczymy korzystac z danego pliku to go zamykamy
    save.close();

    //jak konczymy korzystac z danej tablic to zamykamy obie
    delete [] literki;
    delete[] liczby;

    //koniec programu
    return 0;
}
